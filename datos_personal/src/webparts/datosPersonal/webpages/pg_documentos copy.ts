import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';

export default class searchTemplates 
{

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Documentos del Personal</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: {{vm.currentUserEmail}} iddoc: {{vm.FormdocumentoDataItem.SUBTY}}</label>    
            </div>                
            <table> 

                <tr>
                <td class="${styles.labelTD}">
                    <label>Tipo documento:</label>                    
                </td>
                <td class="${styles.contentTD}"> 
                    <select ng-model="vm.FormdocumentoDataItem.id_TipDoc" disabled="disabled">                        
                        <option ng-repeat = "currentopt in vm.tipodocumCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Nro documento:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.FormdocumentoDataItem.Nro_Doc" disabled="disabled" /></input>
                </td>
                </tr>

                
                <tr>
                <td class="${styles.labelTD}">
                    <label>Pais emisión:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.FormdocumentoDataItem.id_PaisNac" disabled="disabled">
                    <option ng-repeat = "currentopt in vm.paisDocumCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Fecha emisión:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.FormdocumentoDataItem.FchEmision"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Fecha vcto:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.FormdocumentoDataItem.FchVencim"></input>
                </td>
                </tr>
               
            </table>
        </section>
        

        <section>
            <p> Adjuntos:</p>
            <p>{{vm.currentItem.TextoAdjunto}}</p>
            <div>
                <div style="display:inline-flex">
                    <input type="file" id="file-input" data-max-file-size="3MB" ng-upload-change="vm.func_attachEditFiles('file-input', 'files-names')"/>
                    <input type= "button"  value="Elegir Archivos" ng-click="vm.func_clickHTMLElement('file-input')"/>
                </div>
            </div>
            <div ng-show = "vm.attachmentlistDisplay.length == 0">
                <p>No existen archivos adjuntos.</p>
            </div>
            <table id ='files-names'  ng-hide= "vm.attachmentlistDisplay.length == 0">
                <tr ng-repeat="currentobj in vm.attachmentlistDisplay">
                    <td> {{currentobj}} </td>
                    <td><a><img src="https://img.icons8.com/office/16/000000/delete-sign.png" ng-click="vm.func_deleteEditAttachments($index)"></a></td>
                </tr>
            </table>  
        </section>



        <section class = "${styles.buttonsSection}">            
            <button id="POSTbtn" class="${styles.mainButton}" ng-click = "vm.func_GrabarDatosDocum('ACTUALIZA',vm.FormdocumentoDataItem,'','')">Guardar</button>
            <button id="POSTbtnDel" class="${styles.mainButton}" ng-click = "vm.func_EliminaDocumentos(vm.FormdocumentoDataItem.id_documento)">Eliminar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelaDocumento()">Cancelar</button>
        </section>
        </div>
    </div>`;  
    
    




    public static nuevo: string = `<div>
    <div class="${ styles.row }">
          <span class="${ styles.title }">Documentos del Personal</span>
    </div>
    <div class="${styles.content}">
    <section>
        <div class="${styles.subSectionHeading}">
            <label> Ingrese nuevo documento </label>
        </div>
            id empleado: {{vm.g_IDEmpleado}}
        <table> 

            <tr>
            <td class="${styles.labelTD}">
                <label>Tipo documento:</label>                    
            </td>
            <td class="${styles.contentTD}"> 
                <select ng-model="vm.FormdocumentoDataItem.id_TipDoc" >
                    
                    <option ng-repeat = "documopt in vm.tipodocumCollection" value="{{ documopt.coditem }}">{{ documopt.descrip }}</option>
                    
                </select>
            </td>
            </tr>
            

            <tr>
            <td class="${styles.labelTD}">
                <label>Nro documento:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.Nro_Doc"></input>
            </td>
            </tr>

            
            <tr>
            <td class="${styles.labelTD}">
                <label>Pais emisión:</label>                    
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.FormdocumentoDataItem.id_PaisNac">
                
                <option ng-repeat = "documpais in vm.paisDocumCollection" value="{{ documpais.coditem }}">{{ documpais.descrip }}</option>
                </select>
                
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Fecha emisión:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.FchEmision"></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Fecha vcto:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.FchVencim"></input>
            </td>
            </tr>
           
        </table>
    </section>
    
    <section class = "${styles.buttonsSection}">            
            <button id="POSTbtn" class="${styles.mainButton}" ng-click = "vm.func_GrabarDatosDocum('NUEVO',vm.FormdocumentoDataItem, vm.FormdocumentoDataItem.id_TipDoc, vm.FormdocumentoDataItem.id_PaisNac)">Guardar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelaDocumento()">Cancelar</button>
    </section>

        </div>
    </div>`;  




    public static Aprobar: string = `<div>
    <div class="${ styles.row }">
          <span class="${ styles.title }">Documentos del Personal</span>
    </div>
    <div class="${styles.content}">
    <section>
        <div class="${styles.subSectionHeading}">
            <label> Usuario actual: Administrador GH {{vm.g_IdSolicUpdate}} {{vm.FormdocumentoDataItem.SUBTY}}</label>    
        </div>                
        <table> 

            <tr>
            <td class="${styles.labelTD}">
                <label>Tipo documento:</label>                    
            </td>
            <td class="${styles.contentTD}"> 
                <select ng-model="vm.FormdocumentoDataItem.id_TipDoc" disabled="disabled">                        
                    <option ng-repeat = "currentopt in vm.tipodocumCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                </select>
            </td>
            </tr>
            

            <tr>
            <td class="${styles.labelTD}">
                <label>Nro documento:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.Nro_Doc" disabled="disabled" /></input>
            </td>
            </tr>

            
            <tr>
            <td class="${styles.labelTD}">
                <label>Pais emisión:</label>                    
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.FormdocumentoDataItem.id_PaisNac" disabled="disabled">
                <option ng-repeat = "currentopt in vm.paisDocumCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Fecha emisión:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.FchEmision" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Fecha vcto:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.FormdocumentoDataItem.FchVencim" disabled="disabled" /></input>
            </td>
            </tr>
           
        </table>
    </section>
    
    <section class = "${styles.buttonsSection}">            
    <button id="POSTbtnSAPA"  ng-click = "vm.func_AceptaRechazaSolic(vm.FormdocumentoDataItem, vm.g_IdSolicUpdate, 'A', 'frm_formDocumentos_gh', vm.FormdocumentoDataItem.username, vm.FormdocumentoDataItem.SUBTY)">Aceptar</button>
    <button id="POSTbtnSAPR"  ng-click = "vm.func_AceptaRechazaSolic(vm.FormdocumentoDataItem, vm.g_IdSolicUpdate, 'R', 'frm_formDocumentos_gh', vm.FormdocumentoDataItem.username, vm.FormdocumentoDataItem.SUBTY)">Rechazar</button>    
        <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelaDocumento()">Cancelar</button>
    </section>
    </div>
</div>`;  



}