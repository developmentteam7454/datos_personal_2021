import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';
import appController from '../app/appControllerReportes';

export default class reclamoTemplates { 
 

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Datos del Personal 4.79</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Datos personales </label>
            </div>


            <table>
                <tr>
                <td class="${styles.labelTD}">
                    <label>Nombres:</label>
                </td>
                <td class="${styles.contentTD}">
                <input type="text" name="txtNombres" ng-model="vm.personalDataItem.Nombres"/></input>
                       datos: {{vm.personalDataItem.Nombres}}
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Ap Paterno:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['ApPaterno']"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Ap Materno:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['ApMaterno']"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Fecha Nac:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['FchNacimiento']"></input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Pais:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.Pais">
                        <option ng-repeat = "currentopt in vm.paisCollection">{{ currentopt.Title }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Nacionalidad:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_Nacional']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Idioma:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_Idioma']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Estado civil:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_EstCivil']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Religion:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_Relig']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Sexo:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_Sexo']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Telf casa:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['TelfCasa']"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Celular:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['CelPers']"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Grupo sangre:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_GrpSangre']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Area:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.newItem['id_AreaPers']">
                        <option ng-repeat = "option in vm.selectCollection | filter: { SelectName: 'Tipo de Evento' }">{{ option.Title }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Contacto emerg:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['NombreContacEmerg']"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Telf emerg:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.newItem['TelfContacEmerg']"></input>
                </td>
                </tr>


            </table>
        </section>

        <section class = "${styles.buttonsSection}">
            
            <button id="POSTbtn" class="${styles.mainButton}" ng-disabled="vm.func_validateForm()" ng-click = "vm.func_preparePostData()">Guardar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_redirectReportList()">Cancelar</button>
        </section>


        </div>
    </div>`;



}