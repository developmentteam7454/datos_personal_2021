import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';
import appController from '../app/appControllerReportes';

export default class reclamoTemplates { 

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Datos del Personal 10.7</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: {{vm.currentUserEmail}}  {{ vm.g_AreaUsuario}}</label>   
            </div>

            <table>
                <tr>
                <td class="${styles.labelTD}">
                    <label>Nombres:</label>
                </td>
                <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.Nombres" disabled="disabled"/></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Ap Paterno:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.ApPaterno" disabled="disabled" /></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Ap Materno:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.ApMaterno" disabled="disabled" /></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Fecha Nac:</label>
                </td>
                <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.func_parseDateToString(vm.personalDataItem.FchNacimiento)" disabled="disabled" /> </input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Pais:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_PaisNac" disabled="disabled" >                        
                        <option ng-repeat = "currentopt in vm.paisCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Nacionalidad:</label>                    
                </td>
                <td class="${styles.contentTD}">    
                    <select ng-model="vm.personalDataItem.id_Nacional" disabled="disabled" >                        
                        <option ng-repeat = "currentopt in vm.nacionalidadCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Idioma:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_Idioma" disabled="disabled">                        
                        <option ng-repeat = "currentopt in vm.idiomaCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Estado civil:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_EstCivil">                    
                        <option ng-repeat = "currentopt in vm.estcivilCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Religion:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_Relig">                    
                        <option ng-repeat = "currentopt in vm.religionCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Sexo:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_Sexo" disabled="disabled">                    
                        <option ng-repeat = "currentopt in vm.sexoCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Correo:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.correoBol"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Telf casa:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.TelfCasa"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Celular:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.CelPers"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Grupo sangre:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_GrpSangre">                    
                        <option ng-repeat = "currentopt in vm.grpsangreCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Area:</label>
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.personalDataItem.id_AreaPers">
                        <option ng-repeat = "currentopt in vm.areapersCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Contacto emerg:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.NombreContacEmerg"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Telf emerg:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.personalDataItem.TelfContacEmerg"></input>
                </td>
                </tr>


            </table>
        </section>

        <section class = "${styles.buttonsSection}">
            <img src="{{vm.loadingGreen}}" ng-show="vm.waitFlag">

            <button id="POSTbtn" class="${styles.mainButton}"  ng-click = "vm.func_GrabarDatosPersonal(vm.personalDataItem)">Guardar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_redirectReportList()">Cancelar</button>
        </section>


        </div>
    </div>`;








    




    public static Aprobar: string = `<div>
    <div class="${ styles.row }">
          <span class="${ styles.title }">Datos del Personal </span>
    </div>
    <div class="${styles.content}">
    <section>
        <div class="${styles.subSectionHeading}">
            <label> Usuario actual: ADMINISTRADOR GH {{vm.g_IdSolicUpdate}} </label>   
        </div>

        <table>
            <tr>
            <td class="${styles.labelTD}">
                <label>Nombres:</label>
            </td>
            <td class="${styles.contentTD}">
            <input type="text" ng-model="vm.personalDataItem.Nombres" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Ap Paterno:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.ApPaterno" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Ap Materno:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.ApMaterno" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Fecha Nac:</label>
            </td>
            <td class="${styles.contentTD}">
            <input type="text" ng-model="vm.func_parseDateToString(vm.personalDataItem.FchNacimiento)" disabled="disabled" /> </input>
            </td>
            </tr>



            <tr>
            <td class="${styles.labelTD}">
                <label>Pais:</label>
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_PaisNac" disabled="disabled"> 
                    <option ng-repeat = "currentopt in vm.paisCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option>
                </select>
            </td>
            </tr>
            

            <tr>
            <td class="${styles.labelTD}">
                <label>Nacionalidad:</label>                    
            </td>
            <td class="${styles.contentTD}">    
                <select ng-model="vm.personalDataItem.id_Nacional" disabled="disabled">                        
                    <option ng-repeat = "currentopt in vm.nacionalidadCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Idioma:</label>                    
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_Idioma" disabled="disabled">                        
                    <option ng-repeat = "currentopt in vm.idiomaCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Estado civil:</label>                    
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_EstCivil" disabled="disabled">                    
                    <option ng-repeat = "currentopt in vm.estcivilCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>



            <tr>
            <td class="${styles.labelTD}">
                <label>Religion:</label>
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_Relig" disabled="disabled">                    
                    <option ng-repeat = "currentopt in vm.religionCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>



            <tr>
            <td class="${styles.labelTD}">
                <label>Sexo:</label>
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_Sexo" disabled="disabled">                    
                    <option ng-repeat = "currentopt in vm.sexoCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Correo:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.correoBol" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Telf casa:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.TelfCasa" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Celular:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.CelPers" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Grupo sangre:</label>
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_GrpSangre" disabled="disabled">                    
                    <option ng-repeat = "currentopt in vm.grpsangreCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Area:</label>
            </td>
            <td class="${styles.contentTD}">
                <select ng-model="vm.personalDataItem.id_AreaPers" disabled="disabled">
                    <option ng-repeat = "currentopt in vm.areapersCollection" value="{{ currentopt.coditem }}">{{ currentopt.descrip }}</option> 
                </select>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Contacto emerg:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.NombreContacEmerg" disabled="disabled" /></input>
            </td>
            </tr>


            <tr>
            <td class="${styles.labelTD}">
                <label>Telf emerg:</label>
            </td>
            <td class="${styles.contentTD}">
                <input type="text" ng-model="vm.personalDataItem.TelfContacEmerg" disabled="disabled" /></input>
            </td>
            </tr>


        </table>
    </section>

    <section class = "${styles.buttonsSection}">
        <img src="{{vm.loadingGreen}}" ng-show="vm.waitFlag">        
        <button id="POSTbtnSAPA"  ng-click = "vm.func_AceptaRechazaSolic(vm.personalDataItem, vm.g_IdSolicUpdate, 'A', 'frm_datos_personal_gh', vm.personalDataItem.username, '0')">Aceptar</button>
        <button id="POSTbtnSAPR"  ng-click = "vm.func_AceptaRechazaSolic(vm.personalDataItem, vm.g_IdSolicUpdate, 'R', 'frm_datos_personal_gh', vm.personalDataItem.username, '0')">Rechazar</button>       
        <button class="${styles.secondaryButton}" ng-click = "vm.func_redirectReportList()">Cancelar</button>
    </section>


    </div>
    </div>`;
}