import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';


export default class searchTemplates 
{
    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Cuentas CTS</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: {{vm.currentUserEmail}} </label>
                
            </div>

            <table>

                <tr>
                <td class="${styles.labelTD}">
                    <label>País del banco:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.CTSDataItem.id_PaisNac" disabled="disabled">                        
                        <option ng-repeat = "option in vm.paisCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Entidad bancaria:</label>                    
                </td>
                <td class="${styles.contentTD}"> 
                    <select ng-model="vm.CTSDataItem.id_BancoCts">                    
                    <option ng-repeat = "option in vm.bancoctsCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta bancaria:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CTSDataItem.CtaBancaria"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta CCI:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CTSDataItem.CtaCCI"></input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Moneda:</label>                    
                </td>
                <td class="${styles.contentTD}">    
                    <select ng-model="vm.CTSDataItem.id_Moneda">
                    <option ng-repeat = "option in vm.monedaCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>

            </table>
        </section>

        <section class = "${styles.buttonsSection}">
            <button id="POSTbtnSAP"  ng-click = "">Aceptar</button>
            <button id="POSTbtn" class="${styles.mainButton}" ng-click = "vm.func_GrabarDatosCTS(vm.CTSDataItem, vm.CTSDataItem.id_PaisNac, vm.CTSDataItem.id_BancoCts, vm.CTSDataItem.id_Moneda)">Guardar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelarDatos()">Cancelar</button>
        </section>
        </div>
    </div>`;    
 


    public static Aprobar: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Cuentas CTS</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: Administrador GH {{vm.g_IdSolicUpdate}} </label>
                
            </div>

            <table>

                <tr>
                <td class="${styles.labelTD}">
                    <label>País del banco:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.CTSDataItem.id_PaisNac" disabled="disabled">                        
                        <option ng-repeat = "option in vm.paisCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Entidad bancaria:</label>                    
                </td>
                <td class="${styles.contentTD}"> 
                    <select ng-model="vm.CTSDataItem.id_BancoCts" disabled="disabled">                    
                    <option ng-repeat = "option in vm.bancoctsCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta bancaria:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CTSDataItem.CtaBancaria" disabled="disabled" /></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta CCI:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CTSDataItem.CtaCCI" disabled="disabled" /></input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Moneda:</label>                    
                </td>
                <td class="${styles.contentTD}">    
                    <select ng-model="vm.CTSDataItem.id_Moneda" disabled="disabled">
                    <option ng-repeat = "option in vm.monedaCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>


            </table>
        </section>

        <section class = "${styles.buttonsSection}">
        <button id="POSTbtnSAPA"  ng-click = "vm.func_AceptaRechazaSolic(vm.CTSDataItem, vm.g_IdSolicUpdate, 'A', 'frm_cuenta_cts_gh', vm.CTSDataItem.username, '0')">Aceptar</button>
        <button id="POSTbtnSAPR"  ng-click = "vm.func_AceptaRechazaSolic(vm.CTSDataItem, vm.g_IdSolicUpdate, 'R', 'frm_cuenta_cts_gh', vm.CTSDataItem.username, '0')">Rechazar</button>
        
            <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelarDatos()">Cancelar</button>
        </section>


        </div>
    </div>`;    

}
