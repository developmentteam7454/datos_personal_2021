import { Version } from '@microsoft/sp-core-library';

import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField  
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import {
  SPHttpClient,
  SPHttpClientResponse,
} from '@microsoft/sp-http';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './DatosPersonalWebPart.module.scss';
import * as strings from 'DatosPersonalWebPartStrings';

// agregado 12/10/2020 JA
import * as React from 'react';
import * as ReactDom from 'react-dom';
import * as jquery from 'jquery';
import * as angular from 'angular';
import {Deferred} from "ts-deferred";

import './app/appModuleReportes';

// -------- secciones incluida en datos de personal 27/10/2020 JA ---------
import vista_datosPersonales from './webpages/wb_datospersonal';      //  
import vista_documentacion from './webpages/pg_documentos';
import vista_cuentaBancaria from './webpages/pg_cuentasBanco';
import vista_listadocumentos from './webpages/pg_listdocumentos';
import vista_cuentaCTS from './webpages/pg_cuentasCTS';
import vista_listaSolicUpdate from './webpages/pg_listSolicActualizacion';
import vista_espera from './webpages/est_cargando';

import appController from './app/appControllerReportes';

export interface IDatosPersonalWebPartProps {
  description: string;
  formName: string;
}

export default class DatosPersonalWebPart extends BaseClientSideWebPart<IDatosPersonalWebPartProps> {
  
  public obj_sphttpclient: any = null;
  public obj_currentUser:any = null;

  public render(): void 
  {
       
      this.obj_sphttpclient = this.context.spHttpClient;

      //  localStorage.setItem('currentuser',this.context.pageContext.user.email);
      localStorage.setItem('WP_datospers_currentvista',this.properties.formName );

      //Variables para Manejo de Promesa o Carga de Variables Locales
        var defer = new Deferred();
        var pdefer = defer.promise;

      pdefer.then((paramobj:any)=>      
      {        
/*
        var curcontext = paramobj;
                        
              //DATO DE USUARIOS                         
              var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Datos_Personales?code=vzBJu3CAUAtkkd919B5a6Kea18yAEfNMvWwq7bNEtJvYShaiUbuzrQ==&id_persona=2&correoBol=&tip_cons=GH",
                "method": "GET"   
              };
              
              var objreturn = jquery.ajax(settings).done(function (response){
                  //alert(response); 
                var lobjlist = JSON.parse(response); 
                
                var awsdata = [];
                awsdata.push(paramobj); // ..... 0
                awsdata.push(lobjlist); // .... 1
                return awsdata;
                 
              });
          
              return objreturn;
*/return paramobj;

      }).then((paramobj:any)=>{            

            setTimeout(function(){ 
                console.log('Control End');     
              }, 3000);
            
            return paramobj;

      }).then((paramobj:any) =>
      
      {          
        
        var contextobj = paramobj;

        var mainWaitingScreen: string = `<div> Cargando ...</div>`;

        var mainRenderScreen: string = `                            
        
        <div class="${styles.datosPersonal}" data-ng-controller="appControllerReportes as vm"> 
                  
        
              <!--
                SELECCIONE OPCION:
                   
                  <select ng-model="vm.currentView">  
                  <option value="frm_datos_personal">Datos Personal
                  <option value="frm_Listdocumentos">Documentos
                  <option value="frm_cuenta_cts">Cuenta CTS
                  <option value="frm_cuenta_banco">Cuenta Bancaria
                  <option value="frm_ListSolicitudUpgrade">SOLIC. ACTUALIZ.
                  </select> 
                  <br><br>
            -->

          <div class="${styles.container }">
            <div ng-switch ="vm.currentView">
                <div ng-switch-when="frm_datos_personal">
                  <article>  ${vista_datosPersonales.view }   </article>
                </div>
                <div ng-switch-when="frm_Listdocumentos">
                  <article>  ${vista_listadocumentos.view} </article>
                </div>
                <div ng-switch-when="frm_cuenta_cts">
                  <article>  ${vista_cuentaCTS.view}  </article>
                </div>
                <div ng-switch-when="frm_cuenta_banco">
                  <article>  ${vista_cuentaBancaria.view}  </article>
                </div>


                
                <div ng-switch-when="frm_ListSolicitudUpgrade">
                  <article>  ${vista_listaSolicUpdate.view}  </article>
                </div>

                <div ng-switch-when="frm_formDocumentos">
                  <article>  ${vista_documentacion.view}  </article>
                </div>

                <div ng-switch-when="frm_formNuevoDocumentos">
                  <article>  ${vista_documentacion.nuevo }  </article>
                </div>              



                <div ng-switch-when="frm_datos_personal_gh">
                  <article>  ${vista_datosPersonales.Aprobar }   </article>
                </div>
                <div ng-switch-when="frm_cuenta_cts_gh">
                  <article>  ${vista_cuentaCTS.Aprobar }   </article>
                </div>
                <div ng-switch-when="frm_cuenta_banco_gh">
                  <article>  ${vista_cuentaBancaria.Aprobar}  </article>
                </div>
                <div ng-switch-when="frm_formDocumentos_gh">
                  <article>  ${vista_documentacion.Aprobar}  </article>
                </div>


                
                <div ng-switch-when="frm_espera">
                  <article>  ${vista_espera.view}  </article>
                </div>



            </div> 
          </div>

        </div>`;     
        
        contextobj.domElement.innerHTML = mainRenderScreen;
           window['webPartContextDatosPersonalesApp'] = this.context;
        angular.bootstrap(contextobj.domElement, ['appDatosPersonales']);   //  variable definido en Modulo     
                  
      });    
       
      
     
      if (this.renderedOnce == false) {
        this.domElement.innerHTML = `
          <div> Cargando x....</div>`;
        
        defer.resolve(this);
      }  

      
     
  }

  /*
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  */

 




  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneTextField('formName', {
                  label: "Nombre de Formulario"
                }),
                PropertyPaneTextField('listindex', {
                  label: 'Indice de Listas'
                }),
                PropertyPaneTextField('optionlist', {
                  label: 'Lista de Opciones'
                }),
                PropertyPaneTextField('fieldlist', {
                  label: 'Configuración de Campos'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
