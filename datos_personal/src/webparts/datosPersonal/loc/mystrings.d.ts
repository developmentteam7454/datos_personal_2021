declare interface IDatosPersonalWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'DatosPersonalWebPartStrings' {
  const strings: IDatosPersonalWebPartStrings;
  export = strings;
}
