import * as angular from 'angular';
import * as jquery from 'jquery';

import{
  SPHttpClient,
  SPHttpClientResponse,
  ISPHttpClientOptions ,
  ISPHttpClientConfiguration,
  SPHttpClientConfiguration,
  ODataVersion
} from '@microsoft/sp-http';

export interface IListSharePointList
{   value: IItemSharePointList[]; }

export interface IItemSharePointList
{   Title: string;
    Id: string;
}

export interface IDataService
{
    GetTokenAzureAD(user: string): Promise<any>;
    GetDataPerfilAzureAD(user: string): Promise<any>;

    ws_datosusuario(p_user: string): Promise<any>;
    wssap_datosusuario(p_user: string): Promise<any>;
    wssap_cuentacts(p_user: string): Promise<any>;
    wssap_documentosusuario(p_user: string): Promise<any>;
    wssap_cuentabanco(p_user: string): Promise<any>;
    ws_documentosusuario(p_opcion: string, p_IdDoc: string, p_user: string): Promise<any>;
    ws_cuentacts(p_opcion: string, p_iduser: string): Promise<any>;
    ws_cuentabanco(p_opcion: string, p_iduser: string): Promise<any>;
    ws_listacombo_I(p_cmp:string, p_tabla:string): Promise<any>;
    ws_existeRegistroSQL(p_opcion: string, p_IdDoc: string, p_user: string): Promise<any>;
    
    ws_RegistraSolicUpdate(book_pers:any);
    ws_updateDatosPersonal(pBoton:string, pOpcion:string, idPers:string, book_pers:any);
    ws_updateCuentaCTS(pBoton:string, pOper:string, idCTS:string, book:any);
    ws_updateCuentaBancaria(pBoton:string, pOper:string, idCtaBanc:string, book:any);
    ws_updateDocumentos(pBoton:string, pOper:string, idDoc:string, book_doc:any);
    ws_DeleteDocumentos(p_IdDoc: string);

    ws_SolicUpdateDatos(p_opcion: string, p_user: string, p_usergh: string): Promise<any>;
    ws_ApruebaRechazaSolicUpdate(p_idSol:string, p_Operac:string, p_Opcion:string, p_user:string, p_subty:string);

    wssap_PosteaDatosSAP(pURL:string,  book_pers:any);
    
}

export default class DataService implements IDataService 
{
  public render(): void {}
  public static $inject: string[] = ['$q','$http'];

  // AD Azure Credencials:
  private app_ad_client_id     = "f356ba22-a23b-4c2f-b351-62e63d13246c";
  private app_ad_client_secret = "6Z5FLZhKZETyKOse61euF3pDDaj1tMdJwTFo3RTaZ8w=";
  private app_ad_directoryId = "b7e26f48-2292-4a14-a355-1aeb8489ae3d";

  constructor(private $q: angular.IQService, private $http: angular.IHttpService ){   }
  

  public GetTokenAzureAD(user: string): Promise<any> {

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb5h4r3p01nt7454.azurewebsites.net/api/GetUserAzureAD?code=6PQ8FKzSPfWulm1P10Ue2kUPxzrGUUAunCMZ7ZzkqFU7ZjK2GUrYkQ==&username=" + user,
      "method": "GET"
    };
    return jquery.ajax(settings).done((response) => {                
      return response;
    }).fail((response) => {     
        //alert(response); 
      return response;      
    });
  }


  public ws_listacombo_I(p_cmp: string, p_tabla: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_lista_datpers?code=ZNrffueAj/kD83IhDYXFa9slgDDaOZIWQJqsO640488TJDXwUbqEMA==&cmp1=" + p_cmp + "&tabla=" + p_tabla,
      "method": "GET"
    };
    var objreturn = jquery.ajax(settings).done(function (response){          
      //var lobjlist = JSON.parse(response);               
          return response;
      });
      return objreturn;
  }  


  public GetDataPerfilAzureAD(user: string): Promise<any> {

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb5h4r3p01nt7454.azurewebsites.net/api/GetUserAzureAD?code=6PQ8FKzSPfWulm1P10Ue2kUPxzrGUUAunCMZ7ZzkqFU7ZjK2GUrYkQ==&username=" + user,
      "method": "GET"
    };
      var objreturn = jquery.ajax(settings).done(function (response){                
        //alert("DATOS DE perfil usuario..." + response);
        //console.log('SERVICIO ZZZ' + response);
      return response;      
    });
        //alert("perfil..." + objreturn);
      return objreturn;
  }


  public ws_datosusuario(p_user: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Datos_Personales?code=vzBJu3CAUAtkkd919B5a6Kea18yAEfNMvWwq7bNEtJvYShaiUbuzrQ==&cuenta=" + p_user + "&tip_cons=OP",  
      "method": "GET"
    };        
      var objreturn = jquery.ajax(settings).done(function (response){                
        //alert("DATOS DE personal..." + response);
      return response;      
    });
    
    return objreturn;
  }
  

  public ws_existeRegistroSQL(p_opcion: string, p_IdDoc: string, p_user: string): Promise<any> {
    if(p_opcion == "DOCUMENTO")   
    {   // consulta x ID  (Para cada documento en formulario)
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==&subty=" + p_IdDoc + "&cuenta=" + p_user,
        "method": "GET"
      }
    }        
    var objreturn = jquery.ajax(settings).done(function (response){
    //var lobjlist = JSON.parse(response);        
        //alert("DATOS EXISTE DOCUM..." + response);
    return response;
    });
    return objreturn;
  }


  public ws_documentosusuario(p_opcion: string, p_IdDoc: string, p_user: string): Promise<any> {
    if(p_opcion == "ID")   
    {   // consulta x ID  (Para cada documento en formulario)
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==&subty=" + p_IdDoc + "&cuenta=" + p_user,
        "method": "GET"
      }
    }
    else
    {  // consulta x correo (Lista de documentos)
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==&cuenta=" + p_user,
        "method": "GET"
      }
    }    
    var objreturn = jquery.ajax(settings).done(function (response){
    //var lobjlist = JSON.parse(response);        
          //alert("DATOS DE documento..." + response);
        return response;
    });
    return objreturn;
  }




  public ws_SolicUpdateDatos(p_opcion: string, p_user: string, p_usergh: string): Promise<any> {
    if(p_opcion == "USUARIO")   
    {   // consulta x usuario  (Para cada usuario segpun cuenta)
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_SolicUpdateReg?code=/DnxJ8em9HkZFicIdv76eQ0ZgTHKs61gGBX3DTfLNdPUTRzrhIqkYA==&cuenta=" + p_user + "&usergh=" + p_usergh,
        "method": "GET"
      }
    }
    else  // Opcion = TODOS
    {  // consulta total
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_SolicUpdateReg?code=/DnxJ8em9HkZFicIdv76eQ0ZgTHKs61gGBX3DTfLNdPUTRzrhIqkYA==&usergh=" + p_usergh,
        "method": "GET"
      }
    }    
    var objreturn = jquery.ajax(settings).done(function (response){
    //var lobjlist = JSON.parse(response);        
          //alert("DATOS DE documento..." + response);
        return response;
    });
    return objreturn;
  }


  public ws_cuentabanco(p_opcion: string, p_iduser: string): Promise<any> {
    if(p_opcion == "ID")   
    {  // consulta x ID
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Cuenta_Bancaria?code=OaG1BVj7dmi9dMBW2EtdbGypx0zSaergadCHLAEJkCTbFlKgwGPuFg==&id_Pers=" + p_iduser,
        "method": "GET"
      }
    }  
    else
    {   // consulta x correo
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Cuenta_Bancaria?code=OaG1BVj7dmi9dMBW2EtdbGypx0zSaergadCHLAEJkCTbFlKgwGPuFg==&cuenta=" + p_iduser,
        "method": "GET"
      }
    }
    var objreturn = jquery.ajax(settings).done(function (response){     
            //alert("DATOS DE CTA BANCO..." + response);   
        return response;            
    });
        //alert('GET CtaBanco.  opcion: ' + p_opcion + ' / idUSER: ' + p_iduser + '...' + objreturn);
    return objreturn;
  }


  public ws_cuentacts(p_opcion: string, p_iduser: string): Promise<any> {
    if(p_opcion == "ID")   
    {  // consulta x ID  (x ahora es igual q la consulta x CORREO)
      var settings = {
        "async": true,
        "crossDomain": true,
        //"url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_CuentaCTS?code=JWWgyX00D6VZFOAyO2tEgLKrxIj0KsCKk9XdJazBCCZCoZVOAQXUXg==&id_Pers=" + p_iduser,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_CuentaCTS?code=JWWgyX00D6VZFOAyO2tEgLKrxIj0KsCKk9XdJazBCCZCoZVOAQXUXg==&cuenta=" + p_iduser,
        "method": "GET"
      }
    }  
    else
    {   // consulta x correo
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_CuentaCTS?code=JWWgyX00D6VZFOAyO2tEgLKrxIj0KsCKk9XdJazBCCZCoZVOAQXUXg==&cuenta=" + p_iduser,
        "method": "GET"
      }
    }
    var objreturn = jquery.ajax(settings).done(function (response){
      //var lobjlist = JSON.parse(response);    
      //      alert("DATOS DE CTS..." + response);
        return response;
    });        
    return objreturn;
    
  }
  

  public ws_updateDocumentos(pBoton:string, pOper:string, idDoc:string, book_doc: any){
    var req = new XMLHttpRequest();   
    var mensaje ="";
    if(pOper=="ACTUALIZA")  
    {
        var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==&id_Doc=" + idDoc;
        req.open("PUT", servicioGW, true);	
        mensaje ="Documento actualizado exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }  
    else  // NUEVO
    {
        var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==";
        req.open("POST", servicioGW, true);	
        mensaje ="Documento guardado exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("json : " + JSON.stringify(book_doc));
    req.send(JSON.stringify(book_doc));  
    if(pBoton=="BOTON")
    {  alert(mensaje);  }    
  }
  

  public ws_updateCuentaCTS(pBoton:string, pOper:string, idCTS:string, book:any){
    var req = new XMLHttpRequest();    
    var mensaje ="";
    if(pOper=="ACTUALIZA")  
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_CuentaCTS?code=JWWgyX00D6VZFOAyO2tEgLKrxIj0KsCKk9XdJazBCCZCoZVOAQXUXg==&id_cts=" + idCTS;
      req.open("PUT", servicioGW, true);	
      mensaje ="Cuenta CTS actualizada exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    else // NUEVO
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_CuentaCTS?code=JWWgyX00D6VZFOAyO2tEgLKrxIj0KsCKk9XdJazBCCZCoZVOAQXUXg=="
      req.open("POST", servicioGW, true);	
      mensaje ="Cuenta CTS guardada exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("json : " + JSON.stringify(book));
    req.send(JSON.stringify(book)); 
    if(pBoton=="BOTON")
    {  alert(mensaje);  }    
  }



  public ws_updateCuentaBancaria(pBoton:string, pOper:string, idCtaBanc:string, book:any){
    var req = new XMLHttpRequest();    
    var mensaje ="";
    if(pOper=="ACTUALIZA")  
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Cuenta_Bancaria?code=OaG1BVj7dmi9dMBW2EtdbGypx0zSaergadCHLAEJkCTbFlKgwGPuFg==&id_Banco=" + idCtaBanc;
      req.open("PUT", servicioGW, true);	
      mensaje ="Cuenta Bancaria actualizada exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    else // NUEVO
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Cuenta_Bancaria?code=OaG1BVj7dmi9dMBW2EtdbGypx0zSaergadCHLAEJkCTbFlKgwGPuFg=="
      req.open("POST", servicioGW, true);	
      mensaje ="Cuenta Bancaria guardada exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("json : " + JSON.stringify(book));
    req.send(JSON.stringify(book)); 
    if(pBoton=="BOTON")
    {  alert(mensaje);  }
  }


  public ws_updateDatosPersonal(pBoton:string, pOpcion:string, idPers:string, book_pers:any){    
    var req = new XMLHttpRequest();  
    var mensaje ="";          
    if(pOpcion == "ACTUALIZA") 
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Datos_Personales?code=vzBJu3CAUAtkkd919B5a6Kea18yAEfNMvWwq7bNEtJvYShaiUbuzrQ==&id_persona=" + idPers;
      req.open("PUT", servicioGW, true);	
      mensaje ="Datos de personal actualizados exitosamente. El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    else    // pOpcion  = "NUEVO"
    {
      var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Datos_Personales?code=vzBJu3CAUAtkkd919B5a6Kea18yAEfNMvWwq7bNEtJvYShaiUbuzrQ==";
      req.open("POST", servicioGW, true);	
      mensaje ="Datos de personal guardados exitosamente.El Administrador de GH habilitará la visualización de datos a la brevedad.";
    }
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("bodyde datos personal json : " + JSON.stringify(book_pers));
    req.send(JSON.stringify(book_pers));
    if(pBoton=="BOTON")
    {  
      alert(mensaje);  
    }
  }


  public ws_DeleteDocumentos(p_IdDoc: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_Documentos?code=0K761LXcB7w/vA2LsOVFH8KCWgy8vl5gpq/Vp/DiZOuWriQiF/112Q==&id_Doc=" + p_IdDoc,   
      "method": "DELETE"
    };
    var objreturn = jquery.ajax(settings).done(function (response){          
    //var lobjlist = JSON.parse(response);     
        alert("Registro eliminado exitosamente")       
        return response;
    });
    return objreturn;
  }




  public ws_RegistraSolicUpdate(book_pers:any) {    
    var req = new XMLHttpRequest();  
    var mensaje ="";          

    var servicioGW="https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_SolicUpdateReg?code=/DnxJ8em9HkZFicIdv76eQ0ZgTHKs61gGBX3DTfLNdPUTRzrhIqkYA==";
    req.open("POST", servicioGW, true);	
    mensaje ="Datos registrados en solicitud update exitosamente";
  
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("json : " + JSON.stringify(book_pers));
    req.send(JSON.stringify(book_pers));    
        //alert(book_pers);  
    
  }


  public ws_ApruebaRechazaSolicUpdate(p_idSol:string, p_Operac:string, p_Opcion:string, p_user:string, p_subty:string  ){    
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/f_SolicUpdateReg?code=/DnxJ8em9HkZFicIdv76eQ0ZgTHKs61gGBX3DTfLNdPUTRzrhIqkYA==&id_sol="+p_idSol+"&operac="+p_Operac+"&opcion="+p_Opcion+"&user="+p_user+"&subty="+p_subty,   
      "method": "PUT"
    };
    var objreturn = jquery.ajax(settings).done(function (response){          
    //var lobjlist = JSON.parse(response);     
        alert("Solicitud Actualizada exitosamente")       
        return response;
    });
    return objreturn;
  }



  public wssap_datosusuario(p_user: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Datos_Personales?code=sgSlZQHn2hYqP/tJyQ/EtGSBLfjBBgZM4aliisVNa/40srrXSZJzWA==&email=" + p_user,  
      "method": "GET"
    };
      var objreturn = jquery.ajax(settings).done(function (response){   
        //alert("DATOS TRABAJADOR TRAIDOS DE SAP..." + response);                 
      return response;
    });
    return objreturn;
  }


  public wssap_cuentacts(p_user: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_CuentaCTS?code=OxvkuqRzbRiJ7Ahxs/Nbyl8ppiue0KzzAZQnO4jiYqlFjJ5BJeQs5g==&email=" + p_user,  
      "method": "GET"
    };
      var objreturn = jquery.ajax(settings).done(function (response){          
          //alert("DATOS CTS TRAIDOS DE SAP..." + response);
      return response;
    });
    return objreturn;
  }


  public wssap_documentosusuario(p_user: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Documentos?code=T1rxchzVL8VqA/ZVgWUgJWarbaoyRiGEAauCNTb9ZoQS/Wh9mcXs6g==&email=" + p_user,  
      "method": "GET"
    };
      var objreturn = jquery.ajax(settings).done(function (response){          
          //alert("DATOS DOCUM TRAIDOS DE SAP..." + response);
      return response;
    });
    return objreturn;
  }


  public wssap_cuentabanco(p_user: string): Promise<any> {
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Cuenta_Bancaria?code=4VJs2aTDzWygdhzAMTYnrHa148I5U0Pjt5OLo5fYQmmtWPrgsask/A==&email=" + p_user,  
      "method": "GET"
    };
      var objreturn = jquery.ajax(settings).done(function (response){          
          //alert("DATOS CTA BANC desde SAP..." + response);
      return response;
    });
    return objreturn;
  }


  public wssap_PosteaDatosSAP(pURL:string,  book_pers:any){    
    var req = new XMLHttpRequest();  
    var mensaje ="";          

    var servicioGW = pURL;
    req.open("POST", servicioGW, true);	
    mensaje ="Datos registrados en SAP exitosamente";
  
    req.setRequestHeader("accept", "application/json;odata=nometadata"); 
    req.setRequestHeader("Content-Type", "application/json;odata=verbose;charset=UTF-8");
    req.setRequestHeader("odata-version", "");
    req.setRequestHeader("IF-MATCH", "*");          
      //alert("json : " + JSON.stringify(book_pers));
    req.send(JSON.stringify(book_pers));    
      //alert(mensaje);  
  }




  
}