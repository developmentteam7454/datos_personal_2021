import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';
import vista_documentacion from './pg_documentos';

export default class searchTemplates 
{

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Relación de documentos personales</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual SAP: {{vm.currentUserEmail}} </label>
            </div>
            
            <table border="1"  cellpadding="0" cellspacing="0">
                <tr>                                       
                    <th scope="col"> Cod Tipo </th>
                    <th scope="col"> Nro Documento </th>                    
                    <th scope="col"> Fecha Emision </th>
                    <th scope="col"> Fecha Vcto. </th>
                    <th scope="col"> Accion </th>
                </tr>
                
                <tr ng-repeat = "currentobj in vm.documentosSAPDataItem">                                       
                    <td>{{ currentobj.Ictyp }}</td>
                    <td>{{ currentobj.Icnum }}</td>                   
                    <td>{{ vm.func_parseDateToStringSAP(currentobj.Fpdat) }}  </td>
                    <td>{{ vm.func_parseDateToStringSAP(currentobj.Expid) }}</td>
                    <td><a href="#" ng-click="vm.func_ShowFormDocumentos(currentobj)">Editar</a></td>
                </tr>
                
            </table>
        </section>  
        
        <section class = "${styles.buttonsSection}">            
            <button id="POSTbtn" class="${styles.mainButton}" ng-click = "vm.func_ShowFormNuevoDocumento()">Nuevo</button>
            <button class="${styles.mainButton}" ng-click = "vm.func_CancelarDatos()">Cancelar</button>
        </section>


        </div>
    </div>`;    
    

}