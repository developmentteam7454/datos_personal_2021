import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';
import vista_documentacion from './pg_documentos';

export default class searchTemplates 
{

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Relación de Registros Actualizados</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario Administrador </label>
            </div>
            
            <table border="1"  cellpadding="0" cellspacing="0">
                <tr>                                       
                    <th scope="col"> Apellidos </th>
                    <th scope="col"> Nombres  </th>                    
                    <th scope="col"> Usuario </th>
                    <th scope="col"> Descripción </th>
                    <th scope="col"> Fecha </th>
                    <th scope="col"> Accion </th>
                </tr>
                
                <tr ng-repeat = "currentobj in vm.documentosSOLICDataItem">                                       
                    <td>{{ currentobj.Apellidos }}</td>
                    <td>{{ currentobj.nombres }}</td>                   
                    <td>{{ currentobj.username }}</td>  
                    <td>{{ currentobj.descrip }}</td>  
                    <td>{{ vm.func_parseDateToString(currentobj.fechareg) }}</td>
                    <td><a href="#" ng-click="vm.func_DetalleDatosUpdate(currentobj.id_sol, currentobj.campo, currentobj.username, currentobj.subty)">Editar</a></td>
                </tr>
                                                 
            </table>
        </section>  
        
        <section class = "${styles.buttonsSection}">            
            <button class="${styles.secondaryButton}" ng-click = "">Actualizar</button>
            <button class="${styles.secondaryButton}" ng-click = "">Cancelar</button>
        </section>


        </div>
    </div>`;    
    

}