import { IDataService, IListSharePointList, IItemSharePointList } from './appServiceReportes';
import * as angular from 'angular';
import * as jquery from 'jquery';
import {Deferred} from "ts-deferred";

export default class appControllerReportes {

  public currentWebPartContext:any = null;
  public personalDataItem: any = {};
  public personalDataItemSAP: any ={};
  public CTSDataItem: any = {};
  public CtaBancoDataItem: any ={};
  public IdDocumDataItem: any = {};
  public ExisteIdDocumDataItem: any ={};
  public ExistePersonDataItem: any ={};
  public ExisteCTSDataItem: any={};
  public ExisteCtaBancoDataItem: any={};
  public FormdocumentoDataItem: any = {};
  public searchTASAInfoResults: any = {};
  public currentUserEmail: string = null;
  public currentView: string = null;  
  public g_IDEmpleado: bigint=null;
  public g_IDCTS: bigint=null;
  public g_IDCTSPers: bigint=null;
  public g_IDDocumento: bigint=null;
  public g_IDCtaBanco: bigint=null;         
  public g_CUENTAUSER: string =null;
  public currentItem: any = null;
  public g_MensajeCargaWeb: string = null;
  public g_AreaUsuario: any={};
  public g_IdSolicUpdate: bigint=null;
  
  public documentosSOLICDataItem: any[]=[];
  public documentosDataItem: any[] = [];
  public documentosSAPDataItem: any[] = [];
  public paisCollection: any[] = [];
  public paisCTSCollection: any[] = [];
  public paisDocumCollection: any[] = [];
  public monedaCollection: any[] = [];
  public monedaCTSCollection: any[] = [];
  public estcivilCollection: any[] = [];
  public nacionalidadCollection: any[] = [];
  public admpensionCollection: any[] = [];
  public areapersCollection: any[] = [];
  public bancoctsCollection: any[] = [];
  public bancohaberCollection: any[] = [];
  public grpsangreCollection: any[] = [];
  public idiomaCollection: any[] = [];
  public niveleducCollection: any[] = [];
  public religionCollection: any[] = [];
  public sexoCollection: any[] = [];
  public tipocomisCollection: any[] = [];
  public tipodocumCollection: any[] = [];
  public tipoinstituCollection: any[] = [];
  public viaCollection: any[] = [];
  public vincfamiliaCollection: any[] = [];
  public zonaCollection: any[] = [];
  public paisCtaBancoCollection: any[] = [];	
  public bancohaberesCollection: any[] = [];
  public monedaCtaBancoCTSCollection: any[] = [];

  public static $inject: string[] = ['DataService', '$window', '$rootScope','$scope', '$timeout', '$compile'];
  
  // URLs para pasar datos a SAP:
  
  private SAPPersonalData: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Datos_Personales?code=sgSlZQHn2hYqP/tJyQ/EtGSBLfjBBgZM4aliisVNa/40srrXSZJzWA==";
                                     
  private SAPCtaCTS: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_CuentaCTS?code=OxvkuqRzbRiJ7Ahxs/Nbyl8ppiue0KzzAZQnO4jiYqlFjJ5BJeQs5g==";
  private SAPCtaBanco: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Cuenta_Bancaria?code=4VJs2aTDzWygdhzAMTYnrHa148I5U0Pjt5OLo5fYQmmtWPrgsask/A==";
  private SAPDocuments: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/fws_Documentos?code=T1rxchzVL8VqA/ZVgWUgJWarbaoyRiGEAauCNTb9ZoQS/Wh9mcXs6g==";

  private SAPCTSAccount: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/SAPCTSAccountService?code=xKCG1R6qsAjh7Znoexdh0YRi7FJrZO55fds7azaywzE41xMZlPdLfg==&email=LAPOLAYA@TASA.COM.PE";
  private SAPFamily: string = "https://srvweb7454fichad4t0sp3rs0n4ssap.azurewebsites.net/api/SAPFamilyService?code=LaMvQVj8aWjsJK1v3fUjIbHMJ7MKZjJHZw5G6bA6TAvVo6Or1BScOQ==&email=LAPOLAYA@TASA.COM.PE";
  

  constructor(private dataService: IDataService, private $window: angular.IWindowService, private $rootScope: angular.IRootScopeService, private $scope: angular.IScope, private $timeout: angular.ITimeoutService, private $compile: angular.ICompileService)
  {
    const vm: appControllerReportes = this;
        
    this.init();
    console.log(this.currentView);
   }  

   private init(hideFinishedTasks?: boolean): void 
   {        
      this.g_MensajeCargaWeb="CARGANDO INFORMACION...";
      this.func_limpiaCampos();
      this.func_cargacombos();
   

      this.currentView =  localStorage.getItem('WP_datospers_currentvista');

      this.currentUserEmail = 'LAPOLAYA@TASA.COM.PE';      //   descomentar luego....  original debe ser:  this.currentUserEmail        
      
      //"department": "Gestion Humana", ---> El usuario actual pertenece a GH  (x ej  FSEGURA@TASA.COM.PE) 
      // un gestor de GH tiene a su cargo mas de un empleador
      
      this.func_PerfilUsuario(this.currentUserEmail);
      
      
      switch(this.currentView) 
      {
        case "frm_datos_personal":
          {
            //this.func_getUserPersonalDataSAP(this.currentUserEmail);
                        
            this.func_getUserPersonalDataSAP(this.currentUserEmail);  

            this.$timeout((responseObj) =>{
              this.currentView='frm_espera';                  
            },1000);
            
            this.$timeout((responseObj) =>{
              this.currentView='frm_datos_personal';
            },1000);
            
            //this.currentView='frm_datos_personal';

          } break;


        case "frm_cuenta_cts":
          {
            this.func_GetCuentaCTSSAP(this.currentUserEmail);

            this.$timeout((responseObj) =>{
              this.currentView='frm_espera';                  
            },1000);
            
            this.$timeout((responseObj) =>{
              this.currentView='frm_cuenta_cts';
            },1000);

          } break;

        
        case "frm_cuenta_banco":
          { 
            this.func_GetCuentaBancoSAP(this.currentUserEmail); 

            this.$timeout((responseObj) =>{
              this.currentView='frm_espera';                  
            },1000);
            
            this.$timeout((responseObj) =>{
              this.currentView='frm_cuenta_banco';
            },1000);

          } break;

        
        case "frm_Listdocumentos":
          {
            this.func_GetDocumentosSAP("CORREO", '', this.currentUserEmail);

            this.$timeout((responseObj) =>{
              this.currentView='frm_espera';                  
            },1000);
            
            this.$timeout((responseObj) =>{
              this.currentView='frm_Listdocumentos';
            },1000);

          } break;   

          
          case "frm_ListSolicitudUpgrade":
            {
              this.func_GetSolicitudUpdate("TODOS", '');

              this.$timeout((responseObj) =>{
                this.currentView='frm_espera';                  
              },1000);
              
              this.$timeout((responseObj) =>{
                this.currentView='frm_ListSolicitudUpgrade';
              },1000);

            } break; 
      }
      

      
      
          /*
      
      this.func_getUserPersonalDataSAP(this.currentUserEmail);

      this.func_GetCuentaCTSSAP(this.currentUserEmail);
                
      this.func_GetCuentaBancoSAP(this.currentUserEmail);      
            
      this.func_GetDocumentosSAP("CORREO", '', this.currentUserEmail);
                          
      this.func_GetSolicitudUpdate("TODOS", '');
      
      this.g_MensajeCargaWeb="";

      //this.currentView = localStorage.getItem('WP_datospers_currentvista');  //  'frm_datos_personal';
            */

   }

   private func_limpiaCampos()
   {
    this.g_MensajeCargaWeb = null;

    this.personalDataItem = {};
    this.personalDataItemSAP ={};
    this.documentosDataItem = [];
    this.documentosSAPDataItem = [];
    this.CtaBancoDataItem = {};
    this.CTSDataItem = {};
    this.IdDocumDataItem ={};
    this.ExisteIdDocumDataItem={};
    this.ExistePersonDataItem={};
    this.ExisteCTSDataItem={};
    this.ExisteCtaBancoDataItem={};
         
    this.paisCollection = [];
    this.paisCTSCollection = [];
    this.paisDocumCollection = [];
    this.monedaCollection = [];
    this.monedaCTSCollection = [];
    this.estcivilCollection = [];
    this.nacionalidadCollection = [];
    this.admpensionCollection = [];
    this.areapersCollection = [];
    this.bancoctsCollection = [];
    this.bancohaberCollection = [];
    this.grpsangreCollection = [];
    this.idiomaCollection = [];
    this.niveleducCollection = [];
    this.religionCollection = [];
    this.sexoCollection = [];
    this.tipocomisCollection = [];
    this.tipodocumCollection = [];
    this.tipoinstituCollection = [];
    this.viaCollection = [];
    this.vincfamiliaCollection = [];
    this.zonaCollection = [];
    this.paisCtaBancoCollection = [];	
    this.bancohaberesCollection = [];
    this.monedaCtaBancoCTSCollection = [];
    this.documentosSOLICDataItem = [];

    this.currentUserEmail = null;
    this.g_IDEmpleado = null;
    this.g_IDCTS = null;
    this.g_IDCTSPers =null;
    this.g_IDCtaBanco =null;
    this.g_CUENTAUSER =null;
    this.g_IDDocumento = null;
    this.g_AreaUsuario = null;
    this.g_IdSolicUpdate=null;

   }


   private func_cargacombos()
   {      
      this.func_Combo_pais("PAIS","id_paisnac","t_pais");
      this.func_Combo_nacionalidad("NACIONALIDAD","id_nacional","t_nacionalidad");
      this.func_Combo_idioma("IDIOMA","id_Idioma","t_idiomas");
      this.func_Combo_EstCivil("ESTCIVIL","id_estcivil","t_estado_civil");
      this.func_Combo_religion("RELIGION","id_Relig","t_religion");  
      this.func_Combo_sexo("SEXO","id_Sexo","t_sexo"); 
      this.func_Combo_sangre("SANGRE","id_GrpSangre","t_grupo_sanguineo");   
      this.func_Combo_AreaPers("AREAPERS","id_AreaPers","t_area_personal");   
      
      this.func_Combo_PaisBancoCTS("PAISBANCOCTS","id_paisnac","t_pais");   
      this.func_Combo_BancoCTS("BANCOCTS","id_BancoCts","t_banco_cts");   
      this.func_Combo_MonedaCTS("MONEDACTS","id_moneda","t_moneda");   
    
      this.func_Combo_PaisBancoCtaBanco("PAISCTABANCO","id_paisnac","t_pais");   
      this.func_Combo_BancoHaberes("BANCOCTABANCO","id_BancoHab","t_banco_haberes");   
      this.func_Combo_MonedaCtaBanco("MONEDACTABANCO","id_moneda","t_moneda");   

      this.func_Combo_PaisDoc("TIPODOCUM","id_TipDoc","t_tipo_documento");   
      this.func_Combo_tipDoc("PAISDOCUM","id_paisnac","t_pais");
   }


  public func_PerfilUsuario(p_USER:string){    
    this.dataService.GetDataPerfilAzureAD(p_USER)
      .then((TASAResponse) => {
            /*
            console.log("TASA INFO");
            console.log(TASAResponse);
            */
        var TASAJsonResponse = JSON.parse(TASAResponse);        
        this.g_AreaUsuario = TASAJsonResponse.department;
            console.log("AREA USUARIO:" + this.g_AreaUsuario);
        return TASAJsonResponse;
      }).catch((TASAError) => {
        console.log(TASAError);
      });
  }


  public func_getTASAInfo(){
    this.dataService.GetTokenAzureAD(this.currentUserEmail)
      .then((TASAResponse) => {
        var TASAJsonResponse = JSON.parse(TASAResponse);        
        this.g_AreaUsuario = TASAJsonResponse.department;
        //this.searchTASAInfoResults = angular.copy(TASAJsonResponse);
            // console.log("TASA INFO");
            //console.log(TASAJsonResponse);
            
      }).catch((TASAError) => {
        console.log(TASAError);
      });
  }


  // -------------------  INVOCA WEBSRV SAP :  DATOS DATOS TRABAJADOR ------------------

  public func_getUserPersonalData(p_USER:string): void {    
    this.dataService.wssap_datosusuario(p_USER)  
    .then((response)=>{
      this.personalDataItem = JSON.parse(response);
      //this.currentView='frm_datos_personal';           
      //this.g_IDEmpleado = this.personalDataItem.id_persona;
      //this.g_CUENTAUSER = this.personalDataItem.username;
            
         console.log("DESDE SAP: DATOS PERSONALES");
         console.log(response);
         console.log(this.personalDataItem);
              
      return response;
    }).catch((error) => {
      console.log(error);
      return error;
    });


          /*
    this.dataService.ws_datosusuario(p_USER)   
      .then((response)=>{
        this.personalDataItem = JSON.parse(response);
        //this.currentView='frm_datos_personal';           
        this.g_IDEmpleado = this.personalDataItem.id_persona;
        this.g_CUENTAUSER = this.personalDataItem.username;
              
           console.log("CONTROLADOR: DATOS PERSONALES");
           console.log(response);
           console.log(this.personalDataItem);
                
        return response;
      }).catch((error) => {
        console.log(error);
        return error;
      });

            */
  }
  
  
  public func_getUserPersonalDataSAP(p_USER:string): void {
   
    this.g_IDEmpleado=null;
    this.dataService.ws_datosusuario(p_USER)  // verifica los datos desde la BD SQL
    .then((Datoresponse) => {
      this.ExistePersonDataItem = JSON.parse(Datoresponse);
      this.g_IDEmpleado = this.ExistePersonDataItem.id_persona;      
      if(this.g_IDEmpleado<1 || this.g_IDEmpleado==null )
      {
          this.dataService.wssap_datosusuario(p_USER)   // carga datos desde WebSrv SAP
          .then((SAPresponse)=>{
              var jsonResponse = JSON.parse(SAPresponse);                  
              
              var FechaHoy: Date = new Date();
              var body_update = {};                    
              body_update['PERNR'] = jsonResponse.d.Pernr;
              body_update['SUBTY'] = jsonResponse.d.Subty;
              body_update['OBJPS'] = jsonResponse.d.Objps;
              body_update['SPRPS'] = jsonResponse.d.Sprps;
              body_update['ENDDA'] = jsonResponse.d.Endda;
              body_update['BEGDA'] = jsonResponse.d.Begda;
              body_update['SEQNR'] = jsonResponse.d.Seqnr;
              body_update['username'] = jsonResponse.d.Userw;
              body_update['Nombres'] = jsonResponse.d.Vorna;
              body_update['ApPaterno'] = jsonResponse.d.Nachn;
              body_update['ApMaterno'] = jsonResponse.d.Nach2;
              body_update['FchNacimiento'] = this.func_parseStringToDateFromSAP(jsonResponse.d.Gbdat);
              body_update['id_PaisNac'] = jsonResponse.d.Gblnd;
              body_update['id_Nacional'] = jsonResponse.d.Natio;
              body_update['Foto'] = "1";
              body_update['id_Idioma'] = jsonResponse.d.Sprsl;
              body_update['id_EstCivil'] = jsonResponse.d.Famst;
              body_update['id_Relig'] = jsonResponse.d.Konfe;
              body_update['id_Sexo'] = jsonResponse.d.Gesch;
              body_update['correoBol'] = jsonResponse.d.Email;
              body_update['TelfCasa'] = jsonResponse.d.Nrtlf;
              body_update['CelPers'] = jsonResponse.d.Nrcel;
              body_update['id_GrpSangre'] = jsonResponse.d.Gsang;
              body_update['id_AreaPers'] = jsonResponse.d.Persk;
              body_update['NombreContacEmerg'] = "";
              body_update['TelfContacEmerg'] = "";
              body_update['s_fechareg'] = FechaHoy;    
              this.dataService.ws_updateDatosPersonal('',"NUEVO", '', body_update);
          });   
      }
      this.func_getUserPersonalData(p_USER);
             
    });                
    
  }


  public func_getUserPersonalData_GH(p_IdSol:bigint, p_USER:string): void {        
    this.dataService.ws_datosusuario(p_USER)   
      .then((response_gh)=>{
        this.personalDataItem = JSON.parse(response_gh);
        //this.currentView='frm_datos_personal_gh';           
        this.g_IDEmpleado = this.personalDataItem.id_persona;
        this.g_CUENTAUSER = this.personalDataItem.username;
        
              /*
           console.log("CONTROLADOR: DATOS PERSONALES");
           console.log(response);
           console.log(this.personalDataItem);
                */
        return response_gh;
      }).catch((error) => {
        console.log(error);
        return error;
      });
  }



  // -------------------  INVOCA WEBSRV SAP :  DATOS CUENTAS CTS ------------------
  public func_GetCuentaCTS(p_opcion:string, p_user:string ): void {   
      
    this.dataService.wssap_cuentacts(p_user)   
    .then((CTSresponse)=>{      
      this.CTSDataItem = JSON.parse(CTSresponse);   
                      
      return CTSresponse;
    
    }).catch((error) => {
      console.log("ERROR GET DATOS CTS .... " + error);
      return error;
    });

        /*
    this.dataService.ws_cuentacts(p_opcion, p_user)
      .then((CTSresponse)=>{
        
        this.CTSDataItem = JSON.parse(CTSresponse);   
                        
        return CTSresponse;
      }).catch((error) => {
        console.log("ERROR GET DATOS CTS .... " + error);
        return error;
      });

          */
  }
   
  public func_GetCuentaCTSSAP(p_USER:string): void {
    this.g_IDCTS=null;
    this.dataService.ws_cuentacts("CORREO", p_USER)   // verifica los datos desde la BD SQL
    .then((CTSResponse) => {
      this.ExisteCTSDataItem = JSON.parse(CTSResponse);
      this.g_IDCTS = this.ExisteCTSDataItem.id_ctacts;
      if(this.g_IDCTS<1 || this.g_IDCTS==null )    
      {
        this.dataService.wssap_cuentacts(p_USER)   
        .then((SAPCTSresponse)=>{
          var jsonResponse = JSON.parse(SAPCTSresponse);
          
          var FechaHoy: Date = new Date();
          var body_update = {};        
          
          body_update['id_persona'] = 0;  // this.g_IDEmpleado;
          body_update['PERNR'] = jsonResponse.d.Pernr;
          body_update['SUBTY'] = jsonResponse.d.Subty;
          body_update['OBJPS'] = jsonResponse.d.Objps;
          body_update['SPRPS'] = jsonResponse.d.Sprps;
          body_update['ENDDA'] = jsonResponse.d.Endda;
          body_update['BEGDA'] = jsonResponse.d.Begda;
          body_update['SEQNR'] = jsonResponse.d.Seqnr;
          body_update['username'] = jsonResponse.d.Userw;        
          body_update['id_PaisNac'] = jsonResponse.d.Banks;
          body_update['id_BancoCts'] = jsonResponse.d.Bankl;
          body_update['CtaBancaria'] = jsonResponse.d.Bankn;
          body_update['CtaCCI'] = jsonResponse.d.Banci;
          body_update['id_Moneda'] = jsonResponse.d.Waers;
          body_update['s_fechareg'] = FechaHoy;     
          this.dataService.ws_updateCuentaCTS('',"NUEVO", '', body_update);
        });
      }
      //this.currentView='frm_cuenta_cts';      
      this.func_GetCuentaCTS("CORREO",p_USER);
      //this.currentView='frm_cuenta_cts';      
      
    }).catch((error)=>{
      console.log(error);
      return error;
    }); 
 
  }

  
  public func_GetCuentaCTS_GH(p_IdSol:bigint, p_opcion:string, p_user:string ): void {    
    this.dataService.ws_cuentacts(p_opcion, p_user)
      .then((CTSresponse)=>{
        this.CTSDataItem = JSON.parse(CTSresponse);
            //this.g_IdSolicUpdate=p_IdSol; 
            //this.currentView='frm_cuenta_cts_gh';

           //this.g_IDEmpleado = this.personalDataItem.id_persona;
           //this.g_IDCTS = this.CTSDataItem.id_ctacts;
           //this.g_IDCTSPers = this.CTSDataItem.id_persona;                
        return CTSresponse;
      }).catch((error) => {
        console.log(error);
        return error;
      });
  }

  // -------------------  INVOCA WEBSRV SAP :  DATOS CUENTAS BANCARIAS ------------------

  public func_GetCuentaBanco(p_opcion:string,p_user:string ): void {    

    this.dataService.wssap_cuentabanco(p_user)
    .then((BANCresponse)=>{
      this.CtaBancoDataItem = JSON.parse(BANCresponse);
      //this.g_IDCtaBanco = this.CtaBancoDataItem.id_ctabanc;        
         //this.currentView='frm_cuenta_banco';                
      return BANCresponse;
    }).catch((error) => {
      console.log(error);
      return error;
    });

            /*
    this.dataService.ws_cuentabanco(p_opcion, p_user)
      .then((BANCresponse)=>{
        this.CtaBancoDataItem = JSON.parse(BANCresponse);
        this.g_IDCtaBanco = this.CtaBancoDataItem.id_ctabanc;        
           //this.currentView='frm_cuenta_banco';                
        return BANCresponse;
      }).catch((error) => {
        console.log(error);
        return error;
      });
            */
  }


  public func_GetCuentaBancoSAP(p_USER:string): void {    
    this.g_IDCtaBanco=null;
    this.dataService.ws_cuentabanco("CORREO", p_USER)  // verifica los datos desde la BD SQL
    .then((CtaBcoResponse) => {
      this.ExisteCtaBancoDataItem = JSON.parse(CtaBcoResponse);
      this.g_IDCtaBanco = this.ExisteCtaBancoDataItem.id_ctabanc;
      if(this.g_IDCtaBanco <1 || this.g_IDCtaBanco == null ) 
      {
        this.dataService.wssap_cuentabanco(p_USER)
        .then((SAPBancresponse)=>{
          var jsonResponse = JSON.parse(SAPBancresponse);

          var FechaHoy: Date = new Date();
          var body_update = {};        
          
          body_update['id_persona'] = 0;  // this.g_IDEmpleado;
          body_update['PERNR'] = jsonResponse.d.Pernr;
          body_update['SUBTY'] = jsonResponse.d.Subty;
          body_update['OBJPS'] = jsonResponse.d.Objps;
          body_update['SPRPS'] = jsonResponse.d.Sprps;
          body_update['ENDDA'] = jsonResponse.d.Endda;
          body_update['BEGDA'] = jsonResponse.d.Begda;
          body_update['SEQNR'] = jsonResponse.d.Seqnr;
          body_update['username'] = jsonResponse.d.Userw;        
          body_update['id_PaisNac'] = jsonResponse.d.Banks;
          body_update['id_BancoHab'] = jsonResponse.d.Bankl;
          body_update['CtaBancaria'] = jsonResponse.d.Bankn;
          body_update['CtaCCI'] = jsonResponse.d.Banci;
          body_update['id_Moneda'] = jsonResponse.d.Waers;
          body_update['Tipocuenta'] = "";
          body_update['s_fechareg'] = FechaHoy; 
    
          this.dataService.ws_updateCuentaBancaria('', "NUEVO", '', body_update);
        });
      }
      //this.currentView='frm_cuenta_banco';      
      this.func_GetCuentaBanco("CORREO",p_USER);
      
    }).catch((error)=>{
            console.log(error);
            return error;
    });      
  }


  public func_GetCuentaBanco_GH(p_IdSol:bigint, p_opcion:string,p_user:string ): void {    
    this.dataService.ws_cuentabanco(p_opcion, p_user)
      .then((BANCresponse)=>{
        this.CtaBancoDataItem = JSON.parse(BANCresponse);
        this.g_IDCtaBanco = this.CtaBancoDataItem.id_ctabanc;     
        //this.g_IdSolicUpdate=p_IdSol;    
        this.currentView='frm_cuenta_banco_gh';                
        return BANCresponse;
      }).catch((error) => {
        console.log(error);
        return error;
      });
  }

 
  // -------------------  INVOCA WEBSRV SAP :  DATOS DOCUMENTOS ------------------

  public func_GetDocumentos(p_opcion:string, p_Iddoc:string, p_user:string ): void {    
    this.dataService.ws_documentosusuario(p_opcion, p_Iddoc, p_user)
    .then((Docresponse)=>{                  
        this.FormdocumentoDataItem = JSON.parse(Docresponse);  
        //this.g_IDDocumento = this.FormdocumentoDataItem.id_documento
        //this.currentView='frm_formDocumentos';  
        console.log("DOCUMENTOOOOOO... " + Docresponse); 
      return Docresponse;
    }).catch((error) => {
      console.log(error);
      return error;
    });
  }

  public func_GetExisteIdDocumento(p_opcion:string, p_iddoc: string, p_user:string ): void {    
    this.dataService.ws_existeRegistroSQL(p_opcion, p_iddoc, p_user)
      .then((DocIdresponse)=>{
        this.IdDocumDataItem = JSON.parse(DocIdresponse);           
        this.g_IDDocumento = this.IdDocumDataItem.id_documento;           
        //return DocIdresponse;
      }).catch((error) => {
        console.log(error);
        return error;
      });
  }

  public func_GetDocumentosSAP(p_opcion:string, p_Iddoc:string, p_user:string ): void {    
    this.dataService.wssap_documentosusuario(p_user)
      .then((SAPDocresponse)=>{
              
          var jSonDocResponse = JSON.parse(SAPDocresponse);  
          this.documentosSAPDataItem = jSonDocResponse.d.results;
          //this.currentView='frm_Listdocumentos';  
                        
      }).catch((error) => {
      console.log(error);
      return error;      
      });
  }

  
  public func_GetDocumentos_GH(p_IdSol:bigint, p_opcion:string, p_Iddoc:string, p_user:string ): void {    
    this.dataService.ws_documentosusuario(p_opcion, p_Iddoc, p_user)
    .then((Docresponse)=>{                  
        this.FormdocumentoDataItem = JSON.parse(Docresponse);  
        //this.g_IDDocumento = this.FormdocumentoDataItem.id_documento
        this.currentView='frm_formDocumentos_gh';  
                    
      return Docresponse;
    }).catch((error) => {
      console.log(error);
      return error;
    });
  }


  // ------------------ carga lista de solicitudes de upgrade ---------

  public func_GetSolicitudUpdate(p_opcion:string, p_user:string ): void {    
    //this.currentView='frm_ListSolicitudUpgrade';  
    this.dataService.ws_SolicUpdateDatos(p_opcion, p_user, 'LVERA@TASA.COM.PE')
      .then((Solicresponse)=>{              
        this.documentosSOLICDataItem = JSON.parse(Solicresponse);            
        
        console.log("LISTA SOLIC...." + Solicresponse);
        return Solicresponse;                
      }).catch((error) => {
      console.log(error);
      return error;      
      });
  }

  
  // -------------------  CARGA COMBOS ------------------

  public func_Combo_pais(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_pais) => {
      this.paisCollection = JSON.parse(response_pais);        
    }).catch((error) => {
      console.log(error);
    });
  }


  public func_Combo_nacionalidad(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_nacional) => {
      this.nacionalidadCollection = JSON.parse(response_nacional);         
    }).catch((error) => {
      console.log(error);
    });
  }


  public func_Combo_idioma(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_idioma) => {
      this.idiomaCollection = JSON.parse(response_idioma);        
    }).catch((error) => {
      console.log(error);
    });
  }


  public func_Combo_EstCivil(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_estciv) => {
      this.estcivilCollection = JSON.parse(response_estciv);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_religion(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_relig) => {
      this.religionCollection = JSON.parse(response_relig);                
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_sexo(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_sex) => {
      this.sexoCollection = JSON.parse(response_sex);                
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_sangre(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_sangre) => {
      this.grpsangreCollection = JSON.parse(response_sangre);                
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_AreaPers(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_areapers) => {
      this.areapersCollection = JSON.parse(response_areapers);                
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_PaisBancoCTS(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_paiscts) => {
      this.paisCTSCollection = JSON.parse(response_paiscts);
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_BancoCTS(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_bancocts) => {
      this.bancoctsCollection = JSON.parse(response_bancocts);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_MonedaCTS(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_monects) => {
      this.monedaCTSCollection = JSON.parse(response_monects);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_PaisBancoCtaBanco(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_paisctabco) => {
      this.paisCtaBancoCollection = JSON.parse(response_paisctabco);
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_BancoHaberes(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_bancohaber) => {
      this.bancohaberesCollection = JSON.parse(response_bancohaber);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_MonedaCtaBanco(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_monebanco) => {
      this.monedaCtaBancoCTSCollection = JSON.parse(response_monebanco);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_PaisDoc(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_tipodoc) => {
      this.tipodocumCollection = JSON.parse(response_tipodoc);         
    }).catch((error) => {
      console.log(error);
    });
  }

  public func_Combo_tipDoc(p_opcion:string, p_cmp:string, p_tabla:string):void {
    this.dataService.ws_listacombo_I(p_cmp, p_tabla)
    .then((response_paisdoc) => {
      this.paisDocumCollection = JSON.parse(response_paisdoc);          
    }).catch((error) => {
      console.log(error);
    });
  }


   // -------------------- Botón CANCELAR -----
  public func_CancelarDatos()
  {
    this.currentView= 'frm_datos_personal';    
    
          /*
    if(this.currentView == 'home'){
      this.$timeout((responseObj) =>{
        this.func_selectFirstTab();
      },1000);
     }
          */
  }

  // ----- Botón Cancelar edicion documento -----
  public func_CancelaDocumento()
  {
    this.currentView='frm_Listdocumentos';  
  }
  
  // -------------------- Boton de grabar datos de personal --------
  private func_GrabarDatosPersonal(p_ObjPers: any):void {
    
    var pOpcion="";
    var idobjeto = "0";
    var FechaHoy: Date = new Date();
    
      pOpcion="ACTUALIZA";
      var body_update = {};    
      
      body_update['PERNR'] = p_ObjPers.d.Pernr;
      body_update['SUBTY'] = p_ObjPers.d.Subty
      body_update['OBJPS'] = p_ObjPers.d.Objps;
      body_update['SPRPS'] = p_ObjPers.d.Sprps;
      body_update['ENDDA'] = p_ObjPers.d.Endda;
      body_update['BEGDA'] = p_ObjPers.d.Begda;
      body_update['SEQNR'] = p_ObjPers.d.Seqnr;
      body_update['username'] = p_ObjPers.d.Userw;
      body_update['Nombres'] = p_ObjPers.d.Vorna;
      body_update['ApPaterno'] = p_ObjPers.d.Nachn;
      body_update['ApMaterno'] = p_ObjPers.d.Nach2;
      body_update['FchNacimiento'] = this.func_FEchaSQL(p_ObjPers.d.Gbdat);
      body_update['id_PaisNac'] = p_ObjPers.d.Gblnd;
      body_update['id_Nacional'] = p_ObjPers.d.Natio;
      body_update['Foto'] = "1";
      body_update['id_Idioma'] = p_ObjPers.d.Sprsl;
      body_update['id_EstCivil'] = p_ObjPers.d.Famst;
      body_update['id_Relig'] = p_ObjPers.d.Konfe;
      body_update['id_Sexo'] = p_ObjPers.d.Gesch;
      body_update['correoBol'] = p_ObjPers.d.Email;
      body_update['TelfCasa'] = p_ObjPers.d.Nrtlf;
      body_update['CelPers'] = p_ObjPers.d.Nrcel;
      body_update['id_GrpSangre'] = p_ObjPers.d.Gsang;
      body_update['id_AreaPers'] = p_ObjPers.d.Persk;
      body_update['NombreContacEmerg'] = p_ObjPers.d.Nrnrt;
      body_update['TelfContacEmerg'] = p_ObjPers.d.Nrnrc;
      body_update['s_fechareg'] = FechaHoy;    
    
        
    //Guarda datos en SQL Azure
    this.dataService.ws_updateDatosPersonal("BOTON", pOpcion, idobjeto, body_update);

    // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP    
    var body_update_solic = {};        
          
    body_update_solic['PERNR'] = p_ObjPers.d.Pernr;
    body_update_solic['SUBTY'] = p_ObjPers.d.Subty;
    body_update_solic['OBJPS'] = p_ObjPers.d.Objps;
    body_update_solic['SPRPS'] = p_ObjPers.d.Sprps;
    body_update_solic['ENDDA'] = p_ObjPers.d.Endda;
    body_update_solic['BEGDA'] = p_ObjPers.d.Begda;
    body_update_solic['SEQNR'] = p_ObjPers.d.Seqnr;
    body_update_solic['username'] = p_ObjPers.d.Userw;
    body_update_solic['infotipo'] = 'PA0002';
    body_update_solic['campo'] = 'frm_datos_personal_gh';
    body_update_solic['descrip'] = 'Datos personales';
    body_update_solic['fechareg'] = FechaHoy;
    body_update_solic['flg_estatus'] = '';
    body_update_solic['s_fechareg'] = FechaHoy;
    body_update_solic['user_gh'] = 'LVERA@TASA.COM.PE';  // x mientras en duro (tiene q jalar de WS de SAP)

    this.dataService.ws_RegistraSolicUpdate(body_update_solic); 
    
    
    this.$timeout((responseObj) =>{
      this.currentView='frm_espera';                  
    },1000);
    
    this.$timeout((responseObj) =>{
      this.currentView='frm_ListSolicitudUpgrade';
    },1000);
    


        /*
    var pOpcion="";
    var idobjeto = p_ObjPers.id_persona;
    if(idobjeto != 0)
    {
      pOpcion="ACTUALIZA";
      var body_update = {};    
      body_update= angular.copy(p_ObjPers);    
    }
    else  // POR AHORA AQUI NO SE UTILIZA "NUEVO" 
    {
      var FechaHoy: Date = new Date();         
    }
        
    //Guarda datos en SQL Azure
    this.dataService.ws_updateDatosPersonal("BOTON", pOpcion, idobjeto,body_update);

    // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP
    var FechaHoy: Date = new Date();
    var body_update_solic = {};        
          
    body_update_solic['PERNR'] = p_ObjPers['PERNR'];
    body_update_solic['SUBTY'] = p_ObjPers['SUBTY'];
    body_update_solic['OBJPS'] = p_ObjPers['OBJPS'];
    body_update_solic['SPRPS'] = p_ObjPers['SPRPS'];
    body_update_solic['ENDDA'] = p_ObjPers['ENDDA'];
    body_update_solic['BEGDA'] = p_ObjPers['BEGDA'];
    body_update_solic['SEQNR'] = p_ObjPers['SEQNR'];
    body_update_solic['username'] = p_ObjPers['username'];
    body_update_solic['infotipo'] = 'PA0002';
    body_update_solic['campo'] = 'frm_datos_personal_gh';
    body_update_solic['descrip'] = 'Datos personales';
    body_update_solic['fechareg'] = FechaHoy;
    body_update_solic['flg_estatus'] = '';
    body_update_solic['s_fechareg'] = FechaHoy;
    
    this.dataService.ws_RegistraSolicUpdate(body_update_solic); 
          */   
  }


// -------------------- Boton de grabar datos CTS --------
  private func_GrabarDatosCTS(p_ObjCTS: any, p_Pais:string, p_BcoCts:string, p_Mone:string):void {
    
    
      var pOpcion ="ACTUALIZA";    
      var idobjeto = "0";
      var FechaHoy: Date = new Date();
      var body_update = {};          
      
      body_update['id_ctacts'] = '0';
      body_update['id_persona'] = '0';
      body_update['PERNR'] = p_ObjCTS.d.Pernr;
      body_update['SUBTY'] = p_ObjCTS.d.Subty;
      body_update['OBJPS'] = p_ObjCTS.d.Objps;
      body_update['SPRPS'] = p_ObjCTS.d.Sprps;
      body_update['ENDDA'] = p_ObjCTS.d.Endda;
      body_update['BEGDA'] = p_ObjCTS.d.Begda;
      body_update['SEQNR'] = p_ObjCTS.d.Seqnr;
      body_update['username'] = p_ObjCTS.d.Userw;        
      body_update['id_PaisNac'] = p_ObjCTS.d.Banks;
      body_update['id_BancoCts'] = p_ObjCTS.d.Bankl;
      body_update['CtaBancaria'] = p_ObjCTS.d.Bankn;
      body_update['CtaCCI'] = p_ObjCTS.d.Banci;
      body_update['id_Moneda'] = p_ObjCTS.d.Waers;
      body_update['s_fechareg'] = FechaHoy;     

     this.dataService.ws_updateCuentaCTS("BOTON", pOpcion, idobjeto, body_update)
    
      // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP      
      var body_update_solic = {};        
            
      body_update_solic['PERNR'] = p_ObjCTS.d.Pernr;
      body_update_solic['SUBTY'] = p_ObjCTS.d.Subty;
      body_update_solic['OBJPS'] = p_ObjCTS.d.Objps;
      body_update_solic['SPRPS'] = p_ObjCTS.d.Sprps;
      body_update_solic['ENDDA'] = p_ObjCTS.d.Endda;
      body_update_solic['BEGDA'] = p_ObjCTS.d.Begda;
      body_update_solic['SEQNR'] = p_ObjCTS.d.Seqnr;
      body_update_solic['username'] = p_ObjCTS.d.Userw;
      body_update_solic['infotipo'] = 'PA9201';
      body_update_solic['campo'] = 'frm_cuenta_cts_gh';
      body_update_solic['descrip'] = 'Cuenta CTS';
      body_update_solic['fechareg'] = FechaHoy;
      body_update_solic['flg_estatus'] = '';
      body_update_solic['s_fechareg'] = FechaHoy;
      body_update_solic['user_gh'] = 'LVERA@TASA.COM.PE';  // x mientras en duro

      this.dataService.ws_RegistraSolicUpdate(body_update_solic);

      this.$timeout((responseObj) =>{
        this.currentView='frm_espera';                  
      },1000);
      
      this.$timeout((responseObj) =>{
        this.currentView='frm_ListSolicitudUpgrade';
      },1000);

      

              /*
    if(this.g_IDCTS > 0 )
    {
      var pOpcion ="ACTUALIZA";    
      var idobjeto = p_ObjCTS.id_ctacts;
      var body_update = {};          
      body_update= angular.copy(p_ObjCTS);            
    }
    else
    {
      var FechaHoy: Date = new Date();
      var pOpcion ="NUEVO";
            
    }
    this.dataService.ws_updateCuentaCTS("BOTON", pOpcion, idobjeto, body_update)
    
      // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP
      var FechaHoy: Date = new Date();
      var body_update_solic = {};        
            
      body_update_solic['PERNR'] = p_ObjCTS['PERNR'];
      body_update_solic['SUBTY'] = p_ObjCTS['SUBTY'];
      body_update_solic['OBJPS'] = p_ObjCTS['OBJPS'];
      body_update_solic['SPRPS'] = p_ObjCTS['SPRPS'];
      body_update_solic['ENDDA'] = p_ObjCTS['ENDDA'];
      body_update_solic['BEGDA'] = p_ObjCTS['BEGDA'];
      body_update_solic['SEQNR'] = p_ObjCTS['SEQNR'];
      body_update_solic['username'] = p_ObjCTS['username'];
      body_update_solic['infotipo'] = 'PA9201';
      body_update_solic['campo'] = 'frm_cuenta_cts_gh';
      body_update_solic['descrip'] = 'Cuenta CTS';
      body_update_solic['fechareg'] = FechaHoy;
      body_update_solic['flg_estatus'] = '';
      body_update_solic['s_fechareg'] = FechaHoy;
      
      this.dataService.ws_RegistraSolicUpdate(body_update_solic);

              */
  }

  
  // -------------------- Boton de grabar datos de cuenta bancaria --------
  private func_GrabarDatosCuentaBanco(p_ObjCtaBco: any, p_Pais:string, p_BcoCta:string, p_Mone:string):void {
          
    
      var pOpcion ="ACTUALIZA";    
      var idobjeto = "0";
      var FechaHoy: Date = new Date();

      var body_update = {}; 
      
          body_update['id_ctabanc'] = '0';
          body_update['id_persona'] = '0';
          body_update['PERNR'] = p_ObjCtaBco.d.Pernr;
          body_update['SUBTY'] = p_ObjCtaBco.d.Subty;
          body_update['OBJPS'] = p_ObjCtaBco.d.Objps;
          body_update['SPRPS'] = p_ObjCtaBco.d.Sprps;
          body_update['ENDDA'] = p_ObjCtaBco.d.Endda;
          body_update['BEGDA'] = p_ObjCtaBco.d.Begda;
          body_update['SEQNR'] = p_ObjCtaBco.d.Seqnr;
          body_update['username'] = p_ObjCtaBco.d.Userw;        
          body_update['id_PaisNac'] = p_ObjCtaBco.d.Banks;
          body_update['id_BancoHab'] = p_ObjCtaBco.d.Bankl;
          body_update['CtaBancaria'] = p_ObjCtaBco.d.Bankn;
          body_update['CtaCCI'] = p_ObjCtaBco.d.Banci;
          body_update['id_Moneda'] = p_ObjCtaBco.d.Waers;
          body_update['Tipocuenta'] = "";
          body_update['s_fechareg'] = FechaHoy;     

    this.dataService.ws_updateCuentaBancaria("BOTON",pOpcion, idobjeto, body_update)

    
     // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP
     var body_update_solic = {};        
           
     body_update_solic['PERNR'] = p_ObjCtaBco.d.Pernr;
     body_update_solic['SUBTY'] = p_ObjCtaBco.d.Subty;
     body_update_solic['OBJPS'] = p_ObjCtaBco.d.Objps;
     body_update_solic['SPRPS'] = p_ObjCtaBco.d.Sprps;
     body_update_solic['ENDDA'] = p_ObjCtaBco.d.Endda;
     body_update_solic['BEGDA'] = p_ObjCtaBco.d.Begda;
     body_update_solic['SEQNR'] = p_ObjCtaBco.d.Seqnr;
     body_update_solic['username'] = p_ObjCtaBco.d.Userw;   
     body_update_solic['infotipo'] = 'PA0009';
     body_update_solic['campo'] = 'frm_cuenta_banco_gh';
     body_update_solic['descrip'] = 'Cuenta Bancaria';
     body_update_solic['fechareg'] = FechaHoy;
     body_update_solic['flg_estatus'] = '';
     body_update_solic['s_fechareg'] = FechaHoy;
     body_update_solic['user_gh'] = 'LVERA@TASA.COM.PE';  // x mientras en duro

     this.dataService.ws_RegistraSolicUpdate(body_update_solic);
    
     this.$timeout((responseObj) =>{
      this.currentView='frm_espera';                  
    },1000);
    
    this.$timeout((responseObj) =>{
      this.currentView='frm_ListSolicitudUpgrade';
    },1000);


              /*
    if(this.g_IDCtaBanco > 0 )
    {
      var pOpcion ="ACTUALIZA";    
      var idobjeto = p_ObjCtaBco.id_ctabanc;
      var body_update = {};          
      body_update= angular.copy(p_ObjCtaBco);            
    }
    else
    {
      var FechaHoy: Date = new Date();
      var pOpcion ="NUEVO";            
    }
    this.dataService.ws_updateCuentaBancaria("BOTON",pOpcion, idobjeto, body_update)
   
     // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP
     var FechaHoy: Date = new Date();
     var body_update_solic = {};        
           
     body_update_solic['PERNR'] = p_ObjCtaBco['PERNR'];
     body_update_solic['SUBTY'] = p_ObjCtaBco['SUBTY'];
     body_update_solic['OBJPS'] = p_ObjCtaBco['OBJPS'];
     body_update_solic['SPRPS'] = p_ObjCtaBco['SPRPS'];
     body_update_solic['ENDDA'] = p_ObjCtaBco['ENDDA'];
     body_update_solic['BEGDA'] = p_ObjCtaBco['BEGDA'];
     body_update_solic['SEQNR'] = p_ObjCtaBco['SEQNR'];
     body_update_solic['username'] = p_ObjCtaBco['username'];
     body_update_solic['infotipo'] = 'PA0009';
     body_update_solic['campo'] = 'frm_cuenta_banco_gh';
     body_update_solic['descrip'] = 'Cuenta Bancaria';
     body_update_solic['fechareg'] = FechaHoy;
     body_update_solic['flg_estatus'] = '';
     body_update_solic['s_fechareg'] = FechaHoy;
     
     this.dataService.ws_RegistraSolicUpdate(body_update_solic);
                  */
  }


  // -------------------- Boton de grabar Documentos --------

  private func_GrabarDatosDocum(pOpcion:string, p_ObjDoc: any, p_TipDoc:string, p_Pais:string):void {
    if(pOpcion=="ACTUALIZA")
    {
        var idobjeto = p_ObjDoc.id_documento;
        var body_update = {};        
        body_update= angular.copy(p_ObjDoc);    
    }
    else
    {
        var FechaHoy: Date = new Date();
        //  ahora no hace nada             
    }
    this.dataService.ws_updateDocumentos("BOTON",pOpcion,idobjeto,body_update)
    
    // Guarda datos en tabla de registro de solicit  update para luego pasar a sAP
    var FechaHoy: Date = new Date();
    var body_update_solic = {};        
          
    body_update_solic['PERNR'] = p_ObjDoc['PERNR'];
    body_update_solic['SUBTY'] = p_ObjDoc['SUBTY'];
    body_update_solic['OBJPS'] = p_ObjDoc['OBJPS'];
    body_update_solic['SPRPS'] = p_ObjDoc['SPRPS'];
    body_update_solic['ENDDA'] = p_ObjDoc['ENDDA'];
    body_update_solic['BEGDA'] = p_ObjDoc['BEGDA'];
    body_update_solic['SEQNR'] = p_ObjDoc['SEQNR'];
    body_update_solic['username'] = p_ObjDoc['username'];
    body_update_solic['infotipo'] = 'PA0185';
    body_update_solic['campo'] = 'frm_formDocumentos_gh';
    body_update_solic['descrip'] = 'Documento';
    body_update_solic['fechareg'] = FechaHoy;
    body_update_solic['flg_estatus'] = '';
    body_update_solic['s_fechareg'] = FechaHoy;
    
    this.dataService.ws_RegistraSolicUpdate(body_update_solic);    

    this.$timeout((responseObj) =>{
      this.currentView='frm_espera';                  
    },1000);
    
    this.$timeout((responseObj) =>{
      this.currentView='frm_ListSolicitudUpgrade';
    },1000);

  }



  // --------------  Boton Acepta / Rechaza Cambios de datos -------------------
  private func_AceptaRechazaSolic(p_ObjDoc: any, p_idSol:string, p_Operac:string, p_Opcion:string, p_user:string, p_subty:string):void {        

    this.dataService.ws_ApruebaRechazaSolicUpdate(p_idSol, p_Operac, p_Opcion, p_user, p_subty)
      
    if(p_Operac=='A')
    {
      switch(p_Opcion) 
      {
        case "frm_datos_personal_gh": {                 
          this.func_POST_SAPDatosPersonal(p_ObjDoc);
        } break;

        case "frm_cuenta_cts_gh": { 
          this.func_POST_SAPCTS(p_ObjDoc);
        } break; 

        case "frm_cuenta_banco_gh": { 
          this.func_POST_SAPCtaBanco(p_ObjDoc);
        } break;

        case "frm_formDocumentos_gh": { 
          this.func_POST_SAPDocumentos(p_ObjDoc);
        } break;      
      }
    }
    

    this.currentView='frm_ListSolicitudUpgrade';
    this.func_GetSolicitudUpdate("TODOS", '')
    //this.init();
  }

  public func_POST_SAPDatosPersonal(pObjFormDoc:any):void
  {    
    var FechaHoy: Date = new Date();
    var body_post_datos = {};        
          
    body_post_datos['Userw'] = pObjFormDoc['username'];
    body_post_datos['Pernr'] = pObjFormDoc['PERNR'];
    body_post_datos['Photo'] = pObjFormDoc['Foto'];
    body_post_datos['Famst'] = pObjFormDoc['id_EstCivil'];
    body_post_datos['Konfe'] = pObjFormDoc['id_Relig'];
    body_post_datos['Gesch'] = pObjFormDoc['id_Sexo'];
    body_post_datos['Email'] = pObjFormDoc['correoBol'];    
    body_post_datos['Nrnrt'] = '';
    body_post_datos['Nrtlf'] = pObjFormDoc['TelfCasa'];  
    body_post_datos['Nrnrc'] = '';
    body_post_datos['Nrcel'] = pObjFormDoc['CelPers'];
          
    this.dataService.wssap_PosteaDatosSAP(this.SAPPersonalData, body_post_datos)
    .then((response)=>{
      console.log(response);
    }).catch((error) => {
        console.log(error);
    });        
  }

  public func_POST_SAPCTS(pObjFormDoc:any):void
  {
    var FechaHoy: Date = new Date();
    var body_post_datos = {};        
          
    body_post_datos['Userw'] = pObjFormDoc['username'];
    body_post_datos['Pernr'] = pObjFormDoc['PERNR'];
    body_post_datos['Banks'] = pObjFormDoc['id_PaisNac'];
    body_post_datos['Bankl'] = pObjFormDoc['id_BancoCts'];
    body_post_datos['Bankn'] = pObjFormDoc['CtaBancaria'];
    body_post_datos['Banci'] = pObjFormDoc['CtaCCI'];
    body_post_datos['Waers'] = pObjFormDoc['id_Moneda'];    
          
    this.dataService.wssap_PosteaDatosSAP(this.SAPCtaCTS, body_post_datos)
    .then((response)=>{
      console.log(response);
    }).catch((error) => {
        console.log(error);
    }); 
  }


  public func_POST_SAPCtaBanco(pObjFormDoc:any):void
  {
    var FechaHoy: Date = new Date();
    var body_post_datos = {};        
          
    body_post_datos['Userw'] = pObjFormDoc['username'];
    body_post_datos['Pernr'] = pObjFormDoc['PERNR'];
    body_post_datos['Bankl'] = pObjFormDoc['id_BancoHab'];
    body_post_datos['Bankn'] = pObjFormDoc['CtaBancaria'];
    body_post_datos['Banci'] = pObjFormDoc['CtaCCI'];
    body_post_datos['Banks'] = pObjFormDoc['id_PaisNac'];
    body_post_datos['Waers'] = pObjFormDoc['id_Moneda'];    
          
    this.dataService.wssap_PosteaDatosSAP(this.SAPCtaBanco, body_post_datos)
    .then((response)=>{
      console.log(response);
    }).catch((error) => {
        console.log(error);
    }); 
    
  }
  

  public func_POST_SAPDocumentos(pObjFormDoc:any):void
  {
     
    var FechaHoy: Date = new Date();
    var body_post_datos = {};        
          
    body_post_datos['Userw'] = pObjFormDoc['username'];
    body_post_datos['Pernr'] = pObjFormDoc['PERNR'];
    body_post_datos['Subty'] = pObjFormDoc['SUBTY'];
    body_post_datos['Objps'] = pObjFormDoc['OBJPS'];
    body_post_datos['Sprps'] = pObjFormDoc['SPRPS'];
    body_post_datos['Seqnr'] = pObjFormDoc['SEQNR'];
    body_post_datos['Ictyp'] = pObjFormDoc['id_TipDoc'];
    body_post_datos['Fpdat'] = this.func_FEchaSAP(pObjFormDoc['FchEmision']);
    body_post_datos['Expid'] = this.func_FEchaSAP(pObjFormDoc['FchVencim']);       
          
    this.dataService.wssap_PosteaDatosSAP(this.SAPDocuments, body_post_datos)
    .then((response)=>{
      console.log(response);
    }).catch((error) => {
        console.log(error);
    }); 

  }


  // .-----  Boton elimina documento -------
  private func_EliminaDocumentos(p_IdDoc: string):void 
  {        
    this.dataService.ws_DeleteDocumentos(p_IdDoc)
    .then((response)=>{
        console.log(response);
    }).catch((error) => {
        console.log(error);
    });

    this.currentView='frm_Listdocumentos';
    this.func_GetDocumentosSAP("CORREO", '', this.currentUserEmail);
  }

    

// -------------------- funciones varias ----------------
public func_DetalleDatosUpdate(pIdSol:bigint, pOPcion: string, pUser: string, pIdDoc: string):void 
{
  this.g_IdSolicUpdate=pIdSol;
  switch(pOPcion) 
  {
    case "frm_datos_personal_gh": {       
      //this.currentView='frm_datos_personal_gh';       
      this.func_getUserPersonalData_GH(pIdSol,pUser);

      this.$timeout((responseObj) =>{
        this.currentView='frm_espera';                  
      },1000);
      
      this.$timeout((responseObj) =>{
        this.currentView='frm_datos_personal_gh';
      },1000);

    } break;

    case "frm_cuenta_cts_gh": { 
      //this.currentView='frm_cuenta_cts_gh'; 
      this.func_GetCuentaCTS_GH(pIdSol, "CORREO", pUser);

      this.$timeout((responseObj) =>{
        this.currentView='frm_espera';                  
      },1000);
      
      this.$timeout((responseObj) =>{
        this.currentView='frm_cuenta_cts_gh';
      },1000);


    } break; 

    case "frm_cuenta_banco_gh": { 
      //this.currentView='frm_cuenta_banco_gh'; 
      this.func_GetCuentaBanco_GH(pIdSol, "CORREO", pUser);

      this.$timeout((responseObj) =>{
        this.currentView='frm_espera';                  
      },1000);
      
      this.$timeout((responseObj) =>{
        this.currentView='frm_cuenta_banco_gh';
      },1000);


    } break;

    case "frm_formDocumentos_gh": { 
      //this.currentView='frm_formDocumentos_gh'; 
      this.func_GetDocumentos_GH(pIdSol,"ID",pIdDoc, pUser);   

      this.$timeout((responseObj) =>{
        this.currentView='frm_espera';                  
      },1000);
      
      this.$timeout((responseObj) =>{
        this.currentView='frm_formDocumentos_gh';
      },1000);


    } break;
    
  }    
} 


public func_ShowFormNuevoDocumento():void
{
    this.currentView = "frm_formNuevoDocumentos";  
    this.func_Combo_PaisDoc("TIPODOCUM","id_TipDoc","t_tipo_documento");   
    this.func_Combo_tipDoc("PAISDOCUM","id_paisnac","t_pais"); 
} 


public func_ShowFormDocumentos(pObjFormDoc:any):void
{
    this.g_IDDocumento = null;
    var p_idDoc = pObjFormDoc.Subty;
    var p_user = pObjFormDoc.Userw;
    this.dataService.ws_existeRegistroSQL("DOCUMENTO", p_idDoc, p_user)
      .then((DocIdresponse)=>{
        this.ExisteIdDocumDataItem = JSON.parse(DocIdresponse);           
        this.g_IDDocumento = this.ExisteIdDocumDataItem.id_documento;     
        if(this.g_IDDocumento<1 || this.g_IDDocumento == null)            
        {
          var FechaHoy: Date = new Date();
          var body_update = {};     
          body_update['id_persona'] = 0;
          body_update['PERNR'] = pObjFormDoc.Pernr;
          body_update['SUBTY'] = pObjFormDoc.Subty;
          body_update['OBJPS'] = pObjFormDoc.Objps;
          body_update['SPRPS'] = pObjFormDoc.Sprps;
          body_update['ENDDA'] = pObjFormDoc.Endda;
          body_update['BEGDA'] = pObjFormDoc.Begda;
          body_update['SEQNR'] = pObjFormDoc.Seqnr;
          body_update['username'] = pObjFormDoc.Userw;    
          body_update['id_TipDoc'] = pObjFormDoc.Ictyp;
          body_update['Nro_Doc'] = pObjFormDoc.Icnum;
          body_update['id_PaisNac'] = pObjFormDoc.Iscot;
          body_update['FchEmision'] = this.func_parseStringToDateFromSAP(pObjFormDoc.Fpdat);
          body_update['FchVencim'] = this.func_parseStringToDateFromSAP(pObjFormDoc.Expid);
          body_update['s_fechareg'] = FechaHoy;              
          this.dataService.ws_updateDocumentos('',"NUEVO", '', body_update)                     
        }          
        
        //this.currentView = "frm_formDocumentos";    
        this.func_GetDocumentos("ID",p_idDoc, p_user);    
        
        this.$timeout((responseObj) =>{
          this.currentView='frm_espera';                  
        },1000);
        
        this.$timeout((responseObj) =>{
          this.currentView='frm_formDocumentos';
        },1000);


      }).catch((error) => {
        console.log(error);
        return error;
      });

}


  private func_parseDateToString(completeDate: String): string {
    if(completeDate != null){
      var day = completeDate.substring(8,10);
      var month = completeDate.substring(5,7);
      var year = completeDate.substring(0, 4);
      var stringdate: string = day +"/"+ month + "/" + year;    // yyyy-mm-dd  a   dd/mm/yyyy

      return stringdate;
    }
  }


  private func_parseDateToStringSAP(completeDate: String): string {
    if(completeDate != null){
      var day = completeDate.substring(6,8);
      var month = completeDate.substring(4,6);
      var year = completeDate.substring(0, 4);
      var stringdate: string = day +"/"+ month + "/" + year;    // yyyymmdd  a   dd/mm/yyyy

      return stringdate;
    }
  }

  private func_FEchaSQL(completeDate: String): string {
    if(completeDate != null){
      var day = completeDate.substring(6,8);
      var month = completeDate.substring(4,6);
      var year = completeDate.substring(0, 4);
      var stringdate: string = year + "-"+ month + "-" + day;    // yyyymmdd  a   yyyy-mm-dd

      return stringdate;
    }
  }


  private func_FEchaSAP(completeDate: String): string {
    if(completeDate != null){
      var day = completeDate.substring(8,10);
      var month = completeDate.substring(5,7);
      var year = completeDate.substring(0, 4);
      var stringdate: string = year + month + day;    // yyyy-mm-dd  a   yyyymmdd

      return stringdate;
    }
  }



  private func_parseStringToDate(dateToFormat: any): Date{
    var completeDate = dateToFormat.substring(0,10); 
    var convertedDate = new Date(completeDate);
    return convertedDate;
  }

  //Function to format a String date coming from SAP to Date Type
  private func_parseStringToDateFromSAP(dateToFormat: string): Date {
    
    var day = dateToFormat.substring(6,8);
    var month = dateToFormat.substring(4,6);
    var year = dateToFormat.substring(0,4);
    var completeDate: string = year +"/"+ month + "/" + day;
    var convertedDate = new Date(completeDate);

    if(convertedDate.toString() == "Invalid Date"){
      return null;
    }else{
      return convertedDate;
    } 
  }

}
