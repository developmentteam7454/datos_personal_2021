import * as angular from 'angular';

import appControllerReportes from './appControllerReportes';
import appDataServiceReportes from './appServiceReportes';
 
const appReporteDocs: angular.IModule = angular.module('appDatosPersonales', []);  // variable bootstrap

appReporteDocs
  .controller('appControllerReportes', appControllerReportes)  // variable que define controlador
  .service('DataService', appDataServiceReportes)