import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';


export default class searchTemplates 
{
    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Cuenta Bancaria</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: {{vm.currentUserEmail}} </label>
                
            </div>

            <table>

                <tr>
                <td class="${styles.labelTD}">
                    <label>País del banco:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.CtaBancoDataItem.d.Banks" disabled="disabled">                        
                        <option ng-repeat = "option in vm.paisCtaBancoCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Entidad bancaria:</label>                    
                </td>
                <td class="${styles.contentTD}"> 
                    <select ng-model="vm.CtaBancoDataItem.d.Bankl">                    
                    <option ng-repeat = "option in vm.bancohaberesCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta bancaria:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CtaBancoDataItem.d.Bankn"></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta CCI:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CtaBancoDataItem.d.Banci"></input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Moneda:</label>                    
                </td>
                <td class="${styles.contentTD}">    
                    <select ng-model="vm.CtaBancoDataItem.d.Waers">
                    <option ng-repeat = "option in vm.monedaCtaBancoCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>

            </table>
        </section>
        <section class = "${styles.buttonsSection}">
           
            <button id="POSTbtn" class="${styles.mainButton}" ng-click = "vm.func_GrabarDatosCuentaBanco(vm.CtaBancoDataItem, '', '', '')">Guardar</button>
            <button class="${styles.secondaryButton}" ng-click = "vm.func_redirectReportList()">Cancelar</button>
        </section>
        </div>
    </div>`;    
 



    public static Aprobar: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">Cuenta Bancaria</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual: Administrador GH {{vm.g_IdSolicUpdate}} </label>
                
            </div>

            <table>

                <tr>
                <td class="${styles.labelTD}">
                    <label>País del banco:</label>                    
                </td>
                <td class="${styles.contentTD}">
                    <select ng-model="vm.CtaBancoDataItem.id_PaisNac" disabled="disabled">                        
                        <option ng-repeat = "option in vm.paisCtaBancoCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>
                

                <tr>
                <td class="${styles.labelTD}">
                    <label>Entidad bancaria:</label>                    
                </td>
                <td class="${styles.contentTD}"> 
                    <select ng-model="vm.CtaBancoDataItem.id_BancoHab" disabled="disabled">                    
                    <option ng-repeat = "option in vm.bancohaberesCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta bancaria:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CtaBancoDataItem.CtaBancaria" disabled="disabled" /></input>
                </td>
                </tr>


                <tr>
                <td class="${styles.labelTD}">
                    <label>Cta CCI:</label>
                </td>
                <td class="${styles.contentTD}">
                    <input type="text" ng-model="vm.CtaBancoDataItem.CtaCCI" disabled="disabled" /></input>
                </td>
                </tr>



                <tr>
                <td class="${styles.labelTD}">
                    <label>Moneda:</label>                    
                </td>
                <td class="${styles.contentTD}">    
                    <select ng-model="vm.CtaBancoDataItem.id_Moneda" disabled="disabled">
                    <option ng-repeat = "option in vm.monedaCtaBancoCTSCollection" value="{{ option.coditem }}">{{ option.descrip }}</option>
                    </select>
                </td>
                </tr>

                


            </table>
        </section>

        <section class = "${styles.buttonsSection}">
        
        <button id="POSTbtnSAPA"  ng-click = "vm.func_AceptaRechazaSolic(vm.CtaBancoDataItem, vm.g_IdSolicUpdate, 'A', 'frm_cuenta_banco_gh', vm.CtaBancoDataItem.username, '0')">Aceptar</button>
        <button id="POSTbtnSAPR"  ng-click = "vm.func_AceptaRechazaSolic(vm.CtaBancoDataItem, vm.g_IdSolicUpdate, 'R', 'frm_cuenta_banco_gh', vm.CtaBancoDataItem.username, '0')">Rechazar</button>        
            <!-- <button class="${styles.secondaryButton}" ng-click = "vm.func_CancelarDatos()">Cancelar</button> -->
            <button class="${styles.secondaryButton}" ng-click = "">Cancelar</button>
        </section>


        </div>
    </div>`;    

}
