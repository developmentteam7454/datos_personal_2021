import styles from '../DatosPersonalWebPart.module.scss';
import '../app/appModuleReportes';
import * as angular from 'angular';


export default class searchTemplates 
{

    public static view: string = `<div>
        <div class="${ styles.row }">
              <span class="${ styles.title }">CARGANDO DATOS</span>
        </div>
        <div class="${styles.content}">
        <section>
            <div class="${styles.subSectionHeading}">
                <label> Usuario actual SAP: {{vm.currentUserEmail}} </label>
            </div>
            
            CARGANDO INFORMACION, ESPERE POR FAVOR....

        </section>  
        
        <section class = "${styles.buttonsSection}">            
        <button class="${styles.mainButton}" ng-click = "">Actualizar</button>            
        <button class="${styles.mainButton}" ng-click = "">Cancelar</button>
        </section>


        </div>
    </div>`;    
    

}